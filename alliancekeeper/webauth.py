from cgi import escape
from datetime import datetime, timedelta
import re

import cherrypy


from lib.hashing_passwords import check_hash

import alliancekeeper
from alliancekeeper import logger, database, dblogger


SESSION_KEY = '_cp_username'

def check_credentials(username, password, admin_login='0'):
    """Verifies credentials for username and password.
    Returns True and the user group on success or False and no user group"""
    myDB = database.DBConnection()
    userlist = myDB.action("select UserID, UserName, PWHash, AccessLevel, InterfaceLook from users")
    if alliancekeeper.CONFIG['HTTP_HASHED_PASSWORD'] and \
        username.strip().lower() == alliancekeeper.CONFIG['HTTP_USER'].lower() and check_hash(password, alliancekeeper.CONFIG['HTTP_PASS']):
        return True, -2, 4, None
    elif username.strip().lower() == alliancekeeper.CONFIG['HTTP_USER'].lower() and password == alliancekeeper.CONFIG['HTTP_PASS']:
        return True, -2, 4, None
    else:
        for user in userlist:
            if username.strip().lower() == user['UserName'].lower():
                if password == user['PWHash']:
                    return True, user['UserID'], user['AccessLevel'], user['InterfaceLook']
                if check_hash(password, user['PWHash']):
                    return True, user['UserID'], user['AccessLevel'], user['InterfaceLook']
        return False, None, None, None

def check_auth(*args, **kwargs):
    """A tool that looks in config for 'auth.require'. If found and it
    is not None, a login is required and the entry is evaluated as a list of
    conditions that the user must fulfill"""
    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        _session = cherrypy.session.get(SESSION_KEY)

        if _session and (_session['user'] and _session['expiry']) and _session['expiry'] > datetime.now():
            cherrypy.request.login = _session['user']
            for condition in conditions:
                # A condition is just a callable that returns true or false
                if not condition():
                    raise cherrypy.HTTPRedirect(alliancekeeper.CONFIG['HTTP_ROOT'])
        else:
            raise cherrypy.HTTPRedirect(alliancekeeper.CONFIG['HTTP_ROOT'] + "auth/logout")


def requireAuth(*conditions):
    """A decorator that appends conditions to the auth.require config
    variable."""
    def decorate(f):
        if not hasattr(f, '_cp_config'):
            f._cp_config = dict()
        if 'auth.require' not in f._cp_config:
            f._cp_config['auth.require'] = []
        f._cp_config['auth.require'].extend(conditions)
        return f
    return decorate


# Conditions are callables that return True
# if the user fulfills the conditions they define, False otherwise
#
# They can access the current username as cherrypy.request.login
#
# Define those at will however suits the application.

def member_of(groupname):
    def check():
        # replace with actual check if <username> is in <groupname>
        _session = cherrypy.session.get(SESSION_KEY)
        return groupname == _session['access_level']
    return check


def min_access_level(minimum):
    def check():
        _session = cherrypy.session.get(SESSION_KEY)
        if _session['access_level'] < minimum:
            return False
        else:
            return True
    return check

def name_is(reqd_username):
    return lambda: reqd_username == cherrypy.request.login

# These might be handy


def any_of(*conditions):
    """Returns True if any of the conditions match"""
    def check():
        for c in conditions:
            if c():
                return True
        return False
    return check


# By default all conditions are required, but this might still be
# needed if you want to use it inside of an any_of(...) condition
def all_of(*conditions):
    """Returns True if all of the conditions match"""
    def check():
        for c in conditions:
            if not c():
                return False
        return True
    return check

class AuthController(object):

    def on_login(self, user_id, username, access_level):
        """Called on successful login"""

        # Save login to the database
        ip_address = cherrypy.request.headers.get('X-Forwarded-For', cherrypy.request.headers.get('Remote-Addr'))
        host = cherrypy.request.headers.get('Origin')
        user_agent = cherrypy.request.headers.get('User-Agent')

        dblogger.user("%s logged into the web interface" % username, username)


        logger.debug(u"alliancekeeper WebAuth :: %s user '%s' logged into alliancekeeper." % (access_level, username))

    def on_logout(self, username, access_level):
        """Called on logout"""
        logger.debug(u"alliancekeeper WebAuth :: %s User '%s' logged out of alliancekeeper." % (access_level, username))

    def get_loginform(self, username="", msg=""):
        from alliancekeeper.webServe import serve_template
        return serve_template(templatename="login.html", title="Login", username=escape(username, True), msg=msg)

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect(alliancekeeper.HTTP_ROOT + "login")

    @cherrypy.expose
    def login(self, username=None, password=None, remember_me='0', admin_login='0'):
        if not cherrypy.config.get('tools.sessions.on'):
            raise cherrypy.HTTPRedirect(alliancekeeper.HTTP_ROOT + "home")

        if not username and not password:
            return self.get_loginform()

        (vaild_login, user_id, access_level, theme) = check_credentials(username, password, admin_login)

        if vaild_login:

            expiry = datetime.now() + (timedelta(days=30) if remember_me == '1' else timedelta(minutes=60))

            cherrypy.session.regenerate()
            cherrypy.request.login = username
            cherrypy.request.accesslevel = access_level
            cherrypy.session[SESSION_KEY] = {'user_id': user_id,
                                             'user': username,
                                             'access_level': access_level,
                                             'expiry': expiry,
                                             'theme': theme}

            self.on_login(user_id, username, access_level)
            raise cherrypy.HTTPRedirect(alliancekeeper.CONFIG['HTTP_ROOT'])

        elif admin_login == '1':
            logger.debug(u"alliancekeeper WebAuth :: Invalid admin login attempt from '%s'." % username)
            raise cherrypy.HTTPRedirect(alliancekeeper.CONFIG['HTTP_ROOT'])
        else:
            logger.debug(u"alliancekeeper WebAuth :: Invalid login attempt from '%s'." % username)
            return self.get_loginform(username, u"Incorrect username/email or password.")

    @cherrypy.expose
    def logout(self):
        if not cherrypy.config.get('tools.sessions.on'):
            raise cherrypy.HTTPRedirecte(alliancekeeper.CONFIG['HTTP_ROOT'])

        _session = cherrypy.session.get(SESSION_KEY)
        cherrypy.session[SESSION_KEY] = None

        if _session and _session['user']:
            cherrypy.request.login = None
            self.on_logout(_session['user'], _session['access_level'])
        raise cherrypy.HTTPRedirect(alliancekeeper.CONFIG['HTTP_ROOT'] + "auth/login")
