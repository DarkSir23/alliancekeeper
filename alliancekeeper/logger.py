import logging
import os
import threading

from logging import handlers


import alliancekeeper
from alliancekeeper import formatter



class RotatingLogger(object):
    def __init__(self, filename):
        self.filename = filename
        self.filehandler = None
        self.consolehandler = None

    def stopLogger(self):
        l = logging.getLogger('alliancekeeper')
        l.removeHandler(self.filehandler)
        l.removeHandler(self.consolehandler)

    def initLogger(self, loglevel=1):

        l = logging.getLogger('alliancekeeper')
        l.setLevel(logging.DEBUG)

        self.filename = os.path.join(alliancekeeper.CONFIG['LOGDIR'], self.filename)

        filehandler = handlers.RotatingFileHandler(
            self.filename,
            maxBytes=alliancekeeper.CONFIG['LOGSIZE'],
            backupCount=alliancekeeper.CONFIG['LOGFILES'])

        filehandler.setLevel(logging.DEBUG)

        fileformatter = logging.Formatter('%(asctime)s - %(levelname)-7s :: %(message)s', '%d-%b-%Y %H:%M:%S')

        filehandler.setFormatter(fileformatter)
        l.addHandler(filehandler)

        if loglevel:
            consolehandler = logging.StreamHandler()
            if loglevel == 1:
                consolehandler.setLevel(logging.INFO)
            if loglevel == 2:
                consolehandler.setLevel(logging.DEBUG)
            consoleformatter = logging.Formatter('%(asctime)s - %(levelname)s :: %(message)s', '%d-%b-%Y %H:%M:%S')
            consolehandler.setFormatter(consoleformatter)
            l.addHandler(consolehandler)
            self.consolehandler = consolehandler

    @staticmethod
    def log(message, level):

        logger = logging.getLogger('alliancekeeper')

        threadname = threading.currentThread().getName()

        # Ensure messages are correctly encoded
        message = formatter.safe_unicode(message).encode(alliancekeeper.SYS_ENCODING)

        if level != 'DEBUG' or alliancekeeper.LOGLEVEL >= 2:
            # Limit the size of "in-memory" log.
            alliancekeeper.LOGLIST.insert(0, (formatter.now(), level, message))
            if len(alliancekeeper.LOGLIST) > alliancekeeper.CONFIG['LOGLIMIT']:
                del alliancekeeper.LOGLIST[-1]

        message = "%s : %s" % (threadname, message)

        if level == 'DEBUG':
            logger.debug(message)
        elif level == 'INFO':
            logger.info(message)
        elif level == 'WARNING':
            logger.warn(message)
        else:
            logger.error(message)

alliancekeeper_log = RotatingLogger('alliancekeeper.log')


def debug(message):
    alliancekeeper_log.log(message, level='DEBUG')

def info(message):
    alliancekeeper_log.log(message, level='INFO')

def warn(message):
    alliancekeeper_log.log(message, level='WARNING')

def error(message):
    alliancekeeper_log.log(message, level='ERROR')

