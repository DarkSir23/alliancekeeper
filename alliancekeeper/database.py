from __future__ import with_statement

import sqlite3
import threading
import time

import alliancekeeper
from alliancekeeper import logger

db_lock = threading.Lock()

class DBConnection:
    def __init__(self):
        self.connection = sqlite3.connect(alliancekeeper.DBFILE, 20)
        self.connection.row_factory = sqlite3.Row
        self.connection.text_factory = lambda x: unicode(x, "utf-8", "ignore")

    def action(self, query, args=None, suppress=None):
        with db_lock:

            if not query:
                return

            sqlResult = None
            attempt = 0

            while attempt < 5:

                try:
                    if not args:
                        sqlResult = self.connection.execute(query)
                    else:
                        sqlResult = self.connection.execute(query, args)
                    self.connection.commit()
                    break

                except sqlite3.OperationalError as e:
                    if "unable to open database file" in e.message or "database is locked" in e.message:
                        logger.warn('Database Error: %s' % e)
                        logger.debug("Attempted db query: [%s]" % query)
                        attempt += 1
                        if attempt == 5:
                            logger.error("Failed db query: [%s]" % query)
                        else:
                            time.sleep(1)
                    else:
                        logger.error('Database error: %s' % e)
                        logger.error("Failed query: [%s]" % query)
                        raise

                except sqlite3.IntegrityError as e:
                    # we could ignore unique errors in sqlite by using "insert or ignore into" statements
                    # but this would also ignore null values as we can't specify which errors to ignore :-(
                    # The python interface to sqlite only returns english text messages, not error codes
                    msg = e.message
                    msg = msg.lower()
                    if suppress == 'UNIQUE' and ('not unique' in msg or 'unique constraint failed' in msg):
                        if int(alliancekeeper.LOGLEVEL) > 2:
                            logger.debug('Suppressed [%s] %s' % (query, e.message))
                        self.connection.commit()
                        break
                    else:
                        logger.error('Database Integrity error: %s' % e)
                        logger.error("Failed query: [%s]" % query)
                        raise

                except sqlite3.DatabaseError as e:
                    logger.error('Fatal error executing %s :: %s' % (query, e))
                    raise

            return sqlResult

    def match(self, query, args=None):
        try:
            # if there are no results, action() returns None and .fetchone() fails
            sqlResults = self.action(query, args).fetchone()
        except sqlite3.Error:
            return []
        if not sqlResults:
            return []

        return sqlResults

    def select(self, query, args=None):
        try:
            # if there are no results, action() returns None and .fetchall() fails
            sqlResults = self.action(query, args).fetchall()
        except sqlite3.Error:
            return []
        if not sqlResults:
            return []

        return sqlResults

    @staticmethod
    def genParams(myDict):
        return [x + " = ?" for x in myDict.keys()]

    def upsert(self, tableName, valueDict, keyDict):
        changesBefore = self.connection.total_changes

        # genParams = lambda myDict: [x + " = ?" for x in myDict.keys()]

        query = "UPDATE " + tableName + " SET " + ", ".join(self.genParams(valueDict)) + \
                " WHERE " + " AND ".join(self.genParams(keyDict))

        self.action(query, valueDict.values() + keyDict.values())

        if self.connection.total_changes == changesBefore:
            query = "INSERT INTO " + tableName + " (" + ", ".join(valueDict.keys() + keyDict.keys()) + ")" + \
                    " VALUES (" + ", ".join(["?"] * len(valueDict.keys() + keyDict.keys())) + ")"
            self.action(query, valueDict.values() + keyDict.values())