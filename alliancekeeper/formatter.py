import datetime
import os
import re
import shlex
import string
import time
import unicodedata

import alliancekeeper

def safe_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)

def now():
    dtnow = datetime.datetime.now()
    return dtnow.strftime("%Y-%m-%d %H:%M:%S")

def check_int(var, default):
    """
    Return an integer representation of var
    or return default value if var is not integer
    """
    try:
        return int(var)
    except (ValueError, TypeError):
        return default

def plural(var):
    if check_int(var, 0) == 1:
        return ''
    return 's'
