import datetime
import hashlib
import os
import random
import re
import threading
import time
import urllib
import calendar
from shutil import copyfile, rmtree
import json

import cherrypy
import alliancekeeper
import lib.simplejson as simplejson
from cherrypy.lib.static import serve_file, serve_download
from lib.hashing_passwords import make_hash
from mako import exceptions
from mako.lookup import TemplateLookup
from alliancekeeper.webauth import AuthController, requireAuth, member_of, any_of, name_is, min_access_level
from alliancekeeper.formatter import check_int, plural
from alliancekeeper.common import checkRunningJobs, dateToTimestamp, datetimeToTimestamp, timestampToDateTime, currentTime, checkToBool, checkToInt, intToYesNo, getTotal, stringToInt, uni
from alliancekeeper.sessions import get_session_info
from alliancekeeper import dblogger
from alliancekeeper import dbactions, versioncheck, SYS_ENCODING
from alliancekeeper.dbactions import getAlliance, getPlayer

from alliancekeeper import logger, database


def serve_template(templatename, **kwargs):
    interface_dir = os.path.join(str(alliancekeeper.PROG_DIR), 'data/interfaces/')
    template_dir = os.path.join(str(interface_dir), alliancekeeper.CONFIG['HTTP_LOOK'])
    if not os.path.isdir(template_dir):
        logger.error("Unable to locate template [%s], reverting to default" % template_dir)
        alliancekeeper.CONFIG['HTTP_LOOK'] = 'default'
        template_dir = os.path.join(str(interface_dir), alliancekeeper.CONFIG['HTTP_LOOK'])

    _hplookup = TemplateLookup(directories=[template_dir])
    _session = get_session_info()

    try:
        if alliancekeeper.UPDATE_MSG:
            template = _hplookup.get_template("dbupdate.html")
            return template.render(message="Database upgrade in progress, please wait...", title="Database Upgrade", timer=5)
        else:
            template = _hplookup.get_template(templatename)
            return template.render(http_root=alliancekeeper.HTTP_ROOT, _session=_session, **kwargs)
    except Exception:
        return exceptions.html_error_template().render()

class WebInterface(object):

    auth = AuthController()

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("home")

    @cherrypy.expose
    @requireAuth()
    def home(self, msg=None):
        title = "Home"
        logger.debug("Displaying Home for %s" % cherrypy.request.headers["Remote-Addr"])
        return serve_template(templatename="index.html", title=title, msg=msg)

    @cherrypy.expose
    def getIndex(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT AllianceName,PlayerCount,MedalCount,AllianceID from alliances '
        cmd += 'order by AllianceName COLLATE NOCASE'
        rowlist = myDB.select(cmd)
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                if ('showall' in kwargs.keys() and kwargs['showall'] == 1) or not row['AllianceName'].startswith('zzz'):
                    arow = list(row)
                    rows.append(arow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows
            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")
            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
            mydict = {'iTotalDisplayRecords': len(filtered),'iTotalRecords': len(rowlist),'aaData': rows,}
            s = simplejson.dumps(mydict)
            return s

        # THREAD LABELING ######

    @staticmethod
    def label_thread(name=None):
        threadname = threading.currentThread().name
        if "Thread=" in threadname:
            if name:
                threading.currentThread().name = name
            else:
                threading.currentThread().name = "WEBSERVER"

    # USERS ######

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def users(self):
        title = "Users"
        return serve_template(templatename="users.html", title=title)

    @cherrypy.expose
    def getUsers(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT UserID,UserName,AccessLevel,InterfaceLook from users'

        args = []

        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        logger.debug('User Rows: %s' % len(rowlist))
        if len(rowlist):
            for row in rowlist:
                rows.append(list(row))

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth()
    def userPage(self, UserID):
        myDB = database.DBConnection()
        user = myDB.match("SELECT * from users WHERE UserID=?", (UserID,))
        username = user['UserName'].encode(alliancekeeper.SYS_ENCODING)
        return serve_template(
            templatename="user.html", title=urllib.quote_plus(username), user=user)


    @cherrypy.expose
    @requireAuth()
    def editUser(self, UserID=None, msg=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from users WHERE UserID=?', (UserID,))
        if data:
            response = data
            prefs = dbactions.getUserPrefs(userid=data['UserID'])
        else:
            response = {
                'UserID': -1,
                'UserName': "",
                'PWHash': "",
                'InterfaceType': "",
                'InterfaceLook': "",
                'AccessLevel': 0,
            }
            prefs = dbactions.getUserPrefs()
        _session = get_session_info()
        logger.debug('Prefs: %s' % prefs)
        if int(_session['user_id']) == int(UserID):
            title="My Preferences"
        elif response['UserID'] == -1:
            title = "Add User"
        else:
            title = "Edit User"
        return serve_template(templatename="edituser.html", title=title, config=response, msg=msg, prefs=prefs)

    @cherrypy.expose
    @requireAuth()
    def userUpdate(self, userid='', username='', accesslevel='', password='', interfacelook='', **kwargs):
        self.label_thread()
        _session = get_session_info()
        logger.debug(kwargs)
        myDB = database.DBConnection()
        if userid:
            newPWhash = ""
            if int(userid) > -1:
                data = myDB.match('SELECT * from users where UserID=?', (userid,))
                preflist = []
                for item in kwargs.keys():
                    if item.startswith("key"):
                        index = int(item[4:7])
                        if index >= len(preflist):
                            for x in range(len(preflist), index - len(preflist) + 1):
                                preflist.append({})
                        preflist[index]['key'] = kwargs[item].encode("utf-8")
                    if item.startswith("value"):
                        index = int(item[6:9])
                        if index >= len(preflist):
                            for x in range(len(preflist), index - len(preflist) + 1):
                                preflist.append({})
                        preflist[index]['value'] = kwargs[item].encode("utf-8")
                newprefs = {}
                userprefs = dbactions.getUserPrefs(userid=userid)
                for pair in preflist:
                    if len(pair['key']) > 0:
                        try:
                            newprefs[pair['key']] = int(pair['value'])
                        except ValueError:
                            newprefs[pair['key']] = pair['value']

                logger.debug("Prefs: %s" % newprefs)
                for key in newprefs.keys():
                    if key not in userprefs.keys() or userprefs[key] != newprefs[key]:
                        userprefs[key] = newprefs[key]
                if username != '' and accesslevel >= 0:
                    if password == data['PWHash']:
                        newPWhash = password
                    else:
                        if len(password) <= 4:
                            return self.editUser(UserID=userid, msg="Password too short")
                        newPWhash = make_hash(password)
                    controlValueDict = {'UserID': userid}
                    newValueDict = {
                        'UserName': username,
                        'AccessLevel': accesslevel,
                        'PWHash': newPWhash,
                        'InterfaceLook': interfacelook,
                        'Prefs': simplejson.dumps(userprefs),
                    }
                    myDB.upsert('users',newValueDict,controlValueDict)
                    logger.info('%s modified user [%s]' % (_session['user'],username))
                    dblogger.user('Modified User %s' % username, _session['user'], _session['user_id'])
                    dblogger.user('Modified by %s' % _session['user'], username, userid)
                    logger.debug("Userid: %s - Session: %s" % (userid, _session['user_id']))
                    if int(userid) == int(_session['user_id']):
                        logger.debug('Users the same')
                        _session['theme'] = interfacelook
                        _session['user'] = username
            elif int(userid) == -1:
                data = myDB.match('SELECT * from users where UserName=?', (username,))
                if len(data):
                    return self.editUser(UserID=userid, msg="Username already exists")
                else:
                    if len(password) <= 4:
                        return self.editUser(UserID=userid, msg="Password too short")
                    newPWhash = make_hash(password)
                    controlValueDict = {'UserID': None}
                    newValueDict = {
                        'UserName': username,
                        'AccessLevel': accesslevel,
                        'PWHash': newPWhash,
                        'Prefs': simplejson.dumps(dbactions.getUserPrefs())
                    }
                    myDB.upsert('users',newValueDict,controlValueDict)
                    logger.info('%s created user [%s]' % (_session['user'],username))
                    dblogger.user('Created User %s' % username, _session['user'], _session['user_id'])
            raise cherrypy.HTTPRedirect("users")
    # CONFIG ######

    @cherrypy.expose
    @requireAuth(min_access_level(3))
    def config(self):
        self.label_thread()
        http_look_dir = os.path.join(alliancekeeper.PROG_DIR, 'data' + os.sep + 'interfaces')
        http_look_list = [name for name in os.listdir(http_look_dir) if os.path.isdir(os.path.join(http_look_dir, name))]
        config = {
            "http_look_list": http_look_list
        }
        return serve_template(templatename="config.html", title="Settings", config=config)

    @cherrypy.expose
    @requireAuth()
    def configUpdate(self, **kwargs):
        self.label_thread()
        if 'current_tab' in kwargs:
            alliancekeeper.CURRENT_TAB = kwargs['current_tab']

        interface = alliancekeeper.CFG.get('General', 'http_look')
        for key in alliancekeeper.CONFIG_DEFINITIONS.keys():
            item_type, section, default = alliancekeeper.CONFIG_DEFINITIONS[key]
            if key.lower() in kwargs:
                value=kwargs[key.lower()]
                if item_type == 'bool':
                    if not value or value == 'False' or value == '0':
                        value = 0
                    else:
                        value = 1
                elif item_type == 'int':
                    value = check_int(value, default)
                alliancekeeper.CONFIG[key] = value
            else:
                # no key is returned for strings not available in config html page so leave those unchanged
                if key in alliancekeeper.CONFIG_NONWEB or key in alliancekeeper.CONFIG_GIT:
                    pass
                #no key is returned for empty tickboxes...
                elif item_type == 'bool':
                    alliancekeeper.CONFIG[key] = 0
                # or empty string values
                else:
                    alliancekeeper.CONFIG[key] = ''

        alliancekeeper.config_write()
        checkRunningJobs()

        raise cherrypy.HTTPRedirect("config")


    # ALLIANCE ######

    @cherrypy.expose
    @requireAuth()
    def alliancePage(self, AllianceID, msg=None, cmd=None):
        myDB = database.DBConnection()
        alliance = getAlliance(AllianceID)
        alliancename = alliance['AllianceName'].encode(alliancekeeper.SYS_ENCODING)
        alliancelist = myDB.action('SELECT AllianceID,AllianceName from alliances WHERE AllianceID > -1 ORDER BY AllianceName ASC')
        commander = None
        officers = []
        if alliance['CommanderID'] == -1 or alliance['CommanderID'] == None or alliance['CommanderID'] == '':
            pass
        else:
            commander = getPlayer(alliance['CommanderID'])
        if alliance['Officers'] == None or alliance['Officers'] == "":
            pass
        else:
            olist = simplejson.loads(alliance['Officers'])
            for oid in olist:
                officer = getPlayer(oid)
                officers.append(officer)
        return serve_template(
            templatename="alliance.html", title=urllib.quote_plus(alliancename), alliance=alliance, alliancelist=alliancelist, msg=msg, officers=officers, commander=commander, cmd=cmd)

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def editAlliance(self, AllianceID=None, msg=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from alliances WHERE AllianceID=?', (AllianceID,))
        if data:
            response = data
        else:
            response = {
                'AllianceID': -3,
                'AllianceName': "",
                'DateAdded': currentTime(),
                'PlayerCount': 0,
                'MedalCount': 0,
                'CommanderID': -1,
                'Officers': "",
                'WarLeague': "",
            }
        _session = get_session_info()
        if response['AllianceID'] == -3:
            title = "Add Alliance"
        else:
            title = "Edit Alliance"
        return serve_template(templatename="editalliance.html", title=title, alliance=response, msg=msg)

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def allianceUpdate(self, allianceid='', alliancename='', dateadded='', playercount='', medalcount='', commanderid='', warleague=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if allianceid:
            if int(allianceid) > -3:
                data = myDB.match('SELECT * from alliances where AllianceID=?', (allianceid,))
                if alliancename != '':
                    controlValueDict = {'AllianceID': allianceid}
                    newValueDict = {
                        'AllianceName': alliancename,
                        'DateAdded': dateadded,
                        'PlayerCount': playercount,
                        'MedalCount': medalcount,
                        'CommanderID': commanderid,
                        'WarLeague': warleague,
                    }
                    myDB.upsert('alliances',newValueDict,controlValueDict)
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            dblogger.alliance('%s modified from %s to %s' % (key, data[key], newValueDict[key]), _session['user'], allianceid)
                            dblogger.user('Modified %s[%s] from %s to %s' % (data['AllianceName'], key, data[key], newValueDict[key]), _session['user'], _session['user_id'])


            elif int(allianceid) == -3:
                controlValueDict = {'AllianceID': None}
                newValueDict = {
                    'AllianceName': alliancename,
                    'DateAdded': dateadded,
                    'PlayerCount': playercount,
                    'MedalCount': medalcount,
                    'CommanderID': commanderid,
                    'WarLeague': warleague,
                }
                myDB.upsert('alliances',newValueDict,controlValueDict)
                alliancecreated = myDB.match('SELECT * from alliances WHERE AllianceName=? and DateAdded=?', (alliancename, dateadded,))
                if not len(alliancecreated):
                    return self.editAlliance(AllianceID=allianceid, msg="Something went wrong.  Alliance not created.")
                else:
                    dblogger.alliance('Alliance created', _session['user'], alliancecreated['AllianceID'])
                    dblogger.user('Created alliance: %s' % (alliancename),_session['user'], _session['user_id'])
            raise cherrypy.HTTPRedirect("home")

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def movePlayers(self, AllianceID=None, action='', redirect='', destination='', player_table_length='', **args):
        _session = get_session_info()
        myDB = database.DBConnection()
        returnDestination = AllianceID
        if action == "Retire":
            destination = -1
        elif action == "BlackList":
            destination = -2
        if action == "Commander":
            if len(args) == 1:
                for pid in args:
                    commander = getPlayer(pid)
                    alliance = getAlliance(AllianceID)
                    myDB.upsert("alliances", {'CommanderID': pid}, {'AllianceID': AllianceID})
                    msg = "%s set as Commander" % commander['PlayerName']
                    dblogger.player('Made commander of %s' % (alliance['AllianceName']),_session['user'], pid)
                    dblogger.alliance('%s set as commander' % (commander['PlayerName']), _session['user'], AllianceID)
            else:
                msg = "Only one player can be made Commander"
        elif action == "Officer":
            if len(args) > 0:
                addedofficers = []
                currentofficers = []
                alliance = getAlliance(AllianceID)
                if alliance['Officers'] is not None and len(alliance['Officers']) > 1:
                    logger.debug(alliance['Officers'])
                    olist = simplejson.loads(alliance['Officers'])
                    for oid in olist:
                        currentofficers.append(oid)
                for pid in args:
                    officer = getPlayer(pid)
                    if pid not in currentofficers:
                        currentofficers.append(pid)
                        addedofficers.append(officer['PlayerName'])
                        dblogger.player('Made officer of %s' % (alliance['AllianceName']),_session['user'], pid)
                        dblogger.alliance('%s added as officer' % (officer['PlayerName']), _session['user'], AllianceID)
                if len(addedofficers) == 0:
                    msg = "No Officers added"
                else:
                    myDB.upsert("alliances", {'Officers': simplejson.dumps(currentofficers)},
                                {'AllianceID': AllianceID})
                    msg = "Added %s as Officers" % addedofficers
        else:
            movedplayers = []
            for pid in args:
                if not pid == 'player_table_length':
                    player = myDB.match('SELECT * from players WHERE PlayerID=?', (pid,))
                    oldalliance = myDB.match('SELECT * from alliances WHERE AllianceID=?', (player['AllianceID'],))
                    alliance = myDB.match('SELECT * from alliances where AllianceID=?', (destination,))
                    if oldalliance['AllianceID'] != alliance['AllianceID']:
                        myDB.upsert("players", {'AllianceID': destination},{'PlayerID': pid})
                        dblogger.player('Moved from %s to %s' % (oldalliance['AllianceName'], alliance['AllianceName']), _session['user'], pid)
                        dblogger.alliance('Moved %s to %s' % (player['PlayerName'], alliance['AllianceName']),
                                          _session['user'], oldalliance['AllianceID'])
                        dblogger.alliance('Moved %s from %s' % (player['PlayerName'], oldalliance['AllianceName']),
                                          _session['user'], alliance['AllianceID'])
                        movedplayers.append(player['PlayerName'])
            if len(movedplayers) > 0:
                dbactions.updateAlliancePlayers(alliance['AllianceID'])
                dbactions.updateAlliancePlayers(oldalliance['AllianceID'])
                msg = "Moved %s to %s" % (movedplayers, alliance['AllianceName'])
            else:
                msg = "No Players moved."
        if redirect=='alliance':
                return self.alliancePage(AllianceID=returnDestination, msg=msg)
        elif redirect == 'players':
                return self.players(msg=msg)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def removeOfficer(self, AllianceID=None, PlayerID=None):
        _session = get_session_info()
        myDB = database.DBConnection()
        returnDestination = AllianceID
        if AllianceID and PlayerID:
            alliance = getAlliance(AllianceID)
            officer = getPlayer(PlayerID)
            currentofficers = []
            if alliance['Officers'] is not None and len(alliance['Officers']) > 1:
                logger.debug(alliance['Officers'])
                olist = simplejson.loads(alliance['Officers'])
                for oid in olist:
                    currentofficers.append(oid)
                logger.debug("%s - %s" % (uni(officer['PlayerID']),currentofficers))
                if uni(officer['PlayerID']) in currentofficers:
                    currentofficers.remove(uni(officer['PlayerID']))
                    myDB.upsert('alliances',{'Officers': simplejson.dumps(currentofficers)}, {'AllianceID': AllianceID})
                    dblogger.alliance('Removed %s from officer list' % officer['PlayerName'],_session['user'],AllianceID)
                    dblogger.player('Removed from officer list of %s' % alliance['AllianceName'], _session['user'],PlayerID)
                    msg = '%s removed as Officer' % officer['PlayerName']
                else:
                    msg = "Somehow, %s wasn't an Officer to begin with" % officer['PlayerName']
        return self.alliancePage(AllianceID=returnDestination, msg=msg)


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def allianceFunctions(self, AllianceID=None, function='', redirect=''):
        _session = get_session_info()
        myDB = database.DBConnection()
        msg = ''
        if function == 'Refresh':
            if AllianceID:
                dbactions.updateAlliancePlayers(AllianceID)
                msg = 'Alliance Updated'
            else:
                alliancelist = myDB.select("select AllianceID from alliances")
                for id in alliancelist:
                    dbactions.updateAlliancePlayers(id['AllianceID'])
                msg = 'Alliances Updated'
        elif function == 'Delete':
            if _session['access_level'] < 2:
                msg="Access denied.  Deletion not allowed."
                return self.alliancePage(AllianceID=AllianceID, msg=msg)
            alliance = myDB.match("select * from alliances WHERE AllianceID=?", (AllianceID,))
            alliancePlayers = myDB.select('select * from players WHERE AllianceID=?', (AllianceID,))
            if len(alliancePlayers) > 0:
                return self.alliancePage(AllianceID=AllianceID, msg="Players still in Alliance, not deleted")
            else:
                myDB.action("DELETE from alliances WHERE AllianceID=?", (AllianceID,))
                msg="Alliance %s deleted" % (alliance['AllianceName'])
        if redirect == 'Alliance':
            return self.alliancePage(AllianceID=AllianceID, msg=msg)
        elif redirect == 'Home':
            return self.home(msg=msg)


    # PLAYERS ######

    @cherrypy.expose
    @requireAuth()
    def players(self, msg=None):
        myDB = database.DBConnection()
        title = "Players"
        alliancelist = myDB.action('SELECT AllianceID,AllianceName from alliances WHERE AllianceID > -1 ORDER BY AllianceName ASC')

        return serve_template(templatename="players.html", title=title, alliancelist=alliancelist, msg=msg)


    @cherrypy.expose
    def getPlayers(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT PlayerID,AllianceName,PlayerName,HeadQuarters,players.MedalCount,PlayerLevel,LineID,'
        cmd += 'PlayerGunIndex,EventCount,WarCount,players.Note, LineDisplay, WarAverage, MaxSquad from players,alliances WHERE players.AllianceID = alliances.AllianceID'

        args = []
        if kwargs['source'] == "Alliance":
            cmd += ' and players.AllianceID=?'
            args.append(kwargs['AllianceID'])
        if kwargs['source'] == "All":
            cmd += ' and players.AllianceID != ? and players.AllianceID != ?'
            args.append('-2')
            args.append('-1')

        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                temprow = []
                temprow.append(row['PlayerID'])
                temprow.append(row['AllianceName'])
                temprow.append(row['Playername'])
                temprow.append(row['HeadQuarters'])
                temprow.append(row['MedalCount'])
                temprow.append(row['PlayerLevel'])
                temprow.append(row['MaxSquad'])
                line = ''
                if len(row['LineID']) > 0 and len(row['LineDisplay']) > 0:
                    line = '%s - [%s]' % (row['LineDisplay'], row['LineID'])
                elif len(row['LineID']) > 0:
                    line = '[%s]' % row['LineID']
                elif len(row['LineDisplay']) > 0:
                    line = row['LineDisplay']
                temprow.append(line)
                temprow.append(row['PlayerGunIndex'])
                temprow.append(row['EventCount'])
                temprow.append(row['WarCount'])
                temprow.append(row['WarAverage'])
                temprow.append(row['Note'])

                rows.append(temprow)

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth()
    def playerPage(self, PlayerID):
        myDB = database.DBConnection()
        player = dbactions.getPlayer(PlayerID)
        playeralliance = myDB.match("SELECT * from alliances WHERE AllianceID=?", (player['AllianceID'],))
        playername = player['PlayerName'].encode(alliancekeeper.SYS_ENCODING)
        leagues = []
        for league in alliancekeeper.WAR_LEAGUES:
            tempscore = dbactions.getWarAverage(PlayerID=PlayerID,WarLeague=league)
            if tempscore:
                leagues.append({'League': league, 'Average': tempscore})
        return serve_template(
            templatename="player.html", title=urllib.quote_plus(playername), player=player, playeralliance=playeralliance, leagues=leagues)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def editPlayer(self, PlayerID=None, AllianceID=None, msg=None, Return=None, ReturnID=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = dbactions.getPlayer(PlayerID)
        if data['PlayerID'] != -3:
            response = data
        else:
            if not AllianceID:
                AllianceID=-1
            response = {
                'PlayerID': -1,
                'AllianceID': AllianceID,
                'PlayerName': "",
                'LineID': "",
                'LineDisplay': "",
                'PlayerLevel': 0,
                'MedalCount': 0,
                'HeadQuarters': 0,
                'PlayerDateAdded': currentTime(),
                'MaxSquad': 0,
                'Note': ""
            }
        alliances = myDB.action('SELECT AllianceID,AllianceName from alliances')

        _session = get_session_info()
        if response['PlayerID'] == -1:
            title = "Add Player"
        else:
            title = "Edit Player"
        return serve_template(templatename="editplayer.html", title=title, player=response, msg=msg, alliances=alliances, Return=Return, ReturnID=ReturnID)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def playerUpdate(self, playerid='', playername='', confirmplayername=False, allianceid='', hqlevel ='', medals='', level='', lineid='', linedisplay='', playerdateadded='', maxsquad='', note='', Return='None', ReturnID='None'):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if playerid:
            if int(playerid) > -1:
                data = myDB.match('SELECT * from players where PlayerID=?', (playerid,))
                if playername != '':
                    controlValueDict = {'PlayerID': playerid}
                    newValueDict = {
                        'PlayerName': playername,
                        'AllianceID': allianceid,
                        'HeadQuarters': hqlevel,
                        'MedalCount': medals,
                        'PlayerLevel': level,
                        'LineID': lineid,
                        'LineDisplay': linedisplay,
                        'PlayerDateAdded': playerdateadded,
                        'MaxSquad': maxsquad,
                        'Note': note,
                    }
                    myDB.upsert('players',newValueDict,controlValueDict)
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            if str(key) == 'AllianceID':
                                oldalliance = myDB.match('SELECT * from alliances where AllianceID=?', (data['AllianceID'],))
                                newalliance = myDB.match('SELECT * from alliances where AllianceID=?', (allianceid,))
                                dblogger.player('Transferred from %s to %s' % (oldalliance['AllianceName'], newalliance['AllianceName']), _session['user'], playerid)
                                dblogger.user('Transferred %s from %s to %s' % (playername, oldalliance['AllianceName'], newalliance['AllianceName']), _session['user'], _session['user_id'])
                                dblogger.alliance('Transferred %s from %s' % (playername, oldalliance['AllianceName']), _session['user'], newalliance['AllianceID'])
                                dblogger.alliance('Transferred %s to %s' % (playername, newalliance['AllianceName']), _session['user'], oldalliance['AllianceID'])
                                dbactions.updateAlliancePlayers(data['AllianceID'])
                            else:
                                dblogger.player('%s modified from %s to %s' % (key, data[key], newValueDict[key]), _session['user'], playerid)
                                dblogger.user('Modified %s[%s] from %s to %s' % (data['PlayerName'], key, data[key], newValueDict[key]), _session['user'], _session['user_id'])
                    dbactions.updateAlliancePlayers(allianceid)

            elif int(playerid) == -1:
                data = myDB.match('SELECT * from players WHERE PlayerName=? COLLATE NOCASE', (playername,))
                if len(data) and not confirmplayername:
                    return self.editPlayer(PlayerID=playerid, AllianceID=allianceid, msg="Player name already exists, please check the checkbox under \
                        the username if you've verified this user isn't already in the system.")
                else:
                    controlValueDict = {'PlayerID': None}
                    newValueDict = {
                        'PlayerName': playername,
                        'AllianceID': allianceid,
                        'HeadQuarters': hqlevel,
                        'MedalCount': medals,
                        'PlayerLevel': level,
                        'LineID': lineid,
                        'LineDisplay': linedisplay,
                        'PlayerDateAdded': playerdateadded,
                        'MaxSquad': maxsquad,
                        'Note': note,
                    }
                    myDB.upsert('players',newValueDict,controlValueDict)
                    alliance = myDB.match('SELECT * from alliances where AllianceID=?', (allianceid,))
                    playercreated = myDB.match('SELECT * from players WHERE PlayerName=? and PlayerDateAdded=?', (playername, playerdateadded,))
                    if not len(playercreated):
                        return self.editPlayer(PlayerID=playerid, AllianceID=allianceid, msg="Something went wrong.  Player not created.")
                    else:
                        logger.debug('Debug: %s - %s' % (playercreated['PlayerID'],playercreated['PlayerName']))
                        dblogger.player('Created in alliance: %s' % (alliance['AllianceName']), _session['user'], playercreated['PlayerID'])
                        dblogger.user('Created Player %s in alliance: %s' % (playername, alliance['AllianceName']),_session['user'], _session['user_id'])
                        dblogger.alliance('Created Player: %s' % (playername),_session['user'], allianceid)
                dbactions.updateAlliancePlayers(allianceid)
            if Return != "None" and ReturnID!="None":
                if Return == "War":
                    return self.warPage(WarID=int(ReturnID))
                if Return == "AllianceEvent":
                    ReturnEvent,ReturnAlliance = ReturnID.split('|')
                    raise cherrypy.HTTPRedirect("allianceEventPage?EventID=%s&AllianceID=%s#results" % (ReturnEvent,ReturnAlliance))
            raise cherrypy.HTTPRedirect("alliancePage?AllianceID=%s" % allianceid)

    # EVENTS ######

    @cherrypy.expose
    @requireAuth()
    def events(self):
        title = "Events"
        return serve_template(templatename="events.html", title=title)

    @cherrypy.expose
    def getEvents(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT events.EventID,EventStartTime,EventName,EventType,EventTotalScore from events'

        args = []
        if kwargs['source'] == "Alliance":
            cmd = 'SELECT events.EventID,EventStartTime,EventName,EventType,AlliancePoints from events,allianceevents \
            WHERE events.EventID = allianceevents.EventID and allianceevents.AllianceID=?'
            args.append(kwargs['AllianceID'])
        else:
            cmd = 'SELECT events.EventID,EventStartTime,EventName,EventType,EventTotalScore from events'
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                rows.append(list(row))

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    def getEventResults(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT eventresults.PlayerID, players.PlayerName, eventresults.EventPoints, alliances.AllianceName, eventresults.EventID, events.EventName, eventresults.AllianceID, events.EventStartTime FROM eventresults,players,alliances,events WHERE eventresults.PlayerID = players.PlayerID and eventresults.AllianceID = alliances.AllianceID and eventresults.EventID = events.EventID'
        args = []
        if kwargs['source'] == "Alliance":
            cmd += ' and eventresults.AllianceID = ? and eventresults.EventID = ?'
            args.append(kwargs['AllianceID'])
            args.append(kwargs['EventID'])
        elif kwargs['source'] == "Player":
            cmd += ' and eventresults.PlayerID = ?'
            args.append(kwargs['PlayerID'])
        elif kwargs['source'] == "All":
            cmd += ' and eventresults.EventID = ?'
            args.append(kwargs['EventID'])
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                commitrow = myDB.match('SELECT CommittedPoints, Notes FROM eventcommitments WHERE PlayerID=? AND AllianceID=? AND EventID=?', (row['PlayerID'], row['AllianceID'],row['EventID'],))
                if len(commitrow) == 0:
                     commitrow = myDB.match('SELECT CommittedPoints, Notes FROM eventcommitments WHERE PlayerID=? AND EventID=?', (row['PlayerID'],row['EventID'],))
                logger.debug(commitrow)
                if len(commitrow) > 0:
                     commit = commitrow['CommittedPoints']
                else:
                     commit = ''
                temprow = []
                if kwargs['source'] == "Alliance":
                    temprow.append("%s|%s|%s" % (row['EventID'], row['AllianceID'], row['PlayerID']))
                    temprow.append(row['PlayerName'])
                    temprow.append(commit)
                    temprow.append(row['EventPoints'])
                    rows.append(temprow)
                elif kwargs['source'] == "Player":
                    temprow.append("%s|%s|%s" % (row['EventID'], row['AllianceID'], row['PlayerID']))
                    temprow.append(row['EventStartTime'])
                    temprow.append(row['EventName'])
                    temprow.append(row['AllianceName'])
                    temprow.append(commit)
                    temprow.append(row['EventPoints'])
                    temprow.append(row['EventID'])
                    temprow.append(row['AllianceID'])
                    rows.append(temprow)
                elif kwargs['source'] == "All":
                    temprow.append(row['AllianceName'])
                    temprow.append(row['PlayerName'])
                    temprow.append(commit)
                    temprow.append(row['EventPoints'])
                    temprow.append(row['AllianceID'])
                    temprow.append(kwargs['EventID'])
                    temprow.append(row['PlayerID'])
                    rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        # logger.debug(mydict)
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def editEvent(self, EventID=None, msg=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from events WHERE EventID=?', (EventID,))
        if data:
            response = data
        else:
            response = {
                'EventID': -1,
                'TimeStamp': currentTime(),
                'EventName': "",
                'EventType': "",
                'EventStartTime': currentTime(),
                'EventTotalScore': 0,
            }

        _session = get_session_info()
        if response['EventID'] == -1:
            title = "Create Event"
        else:
            title = "Edit Event"
        return serve_template(templatename="editevent.html", title=title, event=response, msg=msg, eventtypes=alliancekeeper.EVENT_TYPES)

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def eventUpdate(self, eventid='', timestamp='', eventname='', eventtype ='', eventstarttime='', eventtotalscore=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if eventid:
            if int(eventid) > -1:
                data = myDB.match('SELECT * from events where EventID=?', (eventid,))
                if eventname != '':
                    controlValueDict = {'EventID': eventid}
                    newValueDict = {
                        'TimeStamp': timestamp,
                        'EventName': eventname,
                        'EventType': eventtype,
                        'EventStartTime': eventstarttime,
                        'EventTotalScore': eventtotalscore,
                    }
                    myDB.upsert('events',newValueDict,controlValueDict)
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            dblogger.user('Event modified %s[%s] from %s to %s' % (data['EventName'], key, data[key], newValueDict[key]), _session['user'], _session['user_id'])
                    dbactions.updateAllianceEvents(eventid)

            elif int(eventid) == -1:
                data = myDB.match('SELECT * from events WHERE EventName=?', (eventname,))
                if len(data):
                    return self.editEvent(EventID=eventid, msg="Event Name Already Exists.")
                else:
                    controlValueDict = {'EventID': None}
                    newValueDict = {
                        'TimeStamp': timestamp,
                        'EventName': eventname,
                        'EventType': eventtype,
                        'EventStartTime': eventstarttime,
                        'EventTotalScore': eventtotalscore,
                    }
                    myDB.upsert('events',newValueDict,controlValueDict)
                    eventcreated = myDB.match('SELECT * from events WHERE EventName=? and TimeStamp=?', (eventname, timestamp,))
                    if not len(eventcreated):
                        return self.editEvent(EventID=eventid, msg="Something went wrong.  Event not created.")
                    else:
                        dblogger.user('Created event "%s"' % (eventname),_session['user'], _session['user_id'])
                        logger.debug('Event: %s' % (eventcreated['EventID']))
                        dbactions.updateAllianceEvents(eventcreated['EventID'])
            raise cherrypy.HTTPRedirect("events")

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def eventresultUpdate(self, eventid='', allianceid='', playerid='', eventpoints =''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if eventid:
            if int(eventid) > -1:
                data = myDB.match('SELECT * from eventresults where EventID=? and AllianceID=? and PlayerID=?', (eventid, allianceid, playerid))
                if eventpoints > -1:
                    controlValueDict = {'EventID': eventid,
                                        'AllianceID': allianceid,
                                        'PlayerID': playerid}
                    newValueDict = {
                        'EventPoints': eventpoints,
                    }
                    myDB.upsert('eventresults',newValueDict,controlValueDict)
                    player = myDB.match('SELECT PlayerName,AllianceName,EventName from players,alliances,events,\
                                        eventresults where eventresults.AllianceID = alliances.AllianceID and \
                                        eventresults.PlayerID = players.PlayerID and \
                                        eventresults.EventID = events.EventID and eventresults.AllianceID = ? \
                                        and eventresults.PlayerID = ? and eventresults.EventID = ?', (allianceid, playerid, eventid))
                    logger.debug(player['PlayerName'])
                    for key, value in newValueDict.iteritems():
                        if not data:
                            dblogger.user('Event points added - Event: %s - Player %s [%s] %s points' % (player['EventName'], player['PlayerName'], player['AllianceName'], newValueDict[key]), _session['user'], _session['user_id'])
                            dblogger.player('Event points added - Event: %s - Alliance %s %s points' % (player['EventName'], player['AllianceName'], newValueDict[key]), _session['user'], playerid)
                            dblogger.alliance('Event points added - Event: %s - Player %s %s points' % (player['EventName'], player['PlayerName'], newValueDict[key]), _session['user'], allianceid)
                        elif uni(newValueDict[key]) != uni(data[key]):
                            dblogger.user('Event points modified - Event: %s  - Player %s [%s] from %s to %s points' % (player['EventName'], player['PlayerName'], player['AllianceName'], data[key], newValueDict[key]), _session['user'], _session['user_id'])
                            dblogger.player('Event points modified - Event: %s  - Alliance %s from %s to %s points' % (player['EventName'], player['AllianceName'], data[key], newValueDict[key]), _session['user'], playerid)
                            dblogger.alliance('Event points modified - Event: %s  - Player %s from %s to %s points' % (player['EventName'], player['PlayerName'], data[key], newValueDict[key]), _session['user'], allianceid)

                    dbactions.updateAllianceEvents(eventid)
                    dbactions.updatePlayer(playerid)

            raise cherrypy.HTTPRedirect("allianceEventPage?EventID=%s&AllianceID=%s#results" % (eventid, allianceid))


    @cherrypy.expose
    def getEventHistory(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        AllianceID = kwargs['AllianceID'] if 'AllianceID' in kwargs.keys() else None
        Count = kwargs['Count'] if 'Count' in kwargs.keys() else 10
        EventType = kwargs['EventType'] if 'EventType' in kwargs.keys() else 'Leaderboard'
        logger.debug('Event Type: %s' % EventType)
        rowlist = dbactions.getEventParticipation(AllianceID=AllianceID, Count=Count, EventType=EventType)
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                rows.append(list(row))

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                logger.debug('Filtered: %s' % (filtered))
                logger.debug('%s - %s' % (iDisplayStart, (iDisplayStart + iDisplayLength)))
                rows = filtered[int(iDisplayStart):(int(iDisplayStart) + int(iDisplayLength))]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s


    @cherrypy.expose
    @requireAuth()
    def allianceEventPage(self, EventID, AllianceID):
        myDB=database.DBConnection()
        allianceevent = myDB.match("SELECT * from allianceevents WHERE AllianceID=? and EventID=?", (AllianceID, EventID,))
        eventplan = myDB.match("SELECT * from eventplanning WHERE AllianceID=? and EventID=?", (AllianceID, EventID,))
        alliance = myDB.match("SELECT * from alliances WHERE AllianceID=?", (AllianceID,))
        event = myDB.match("SELECT * from events WHERE EventID=?", (EventID,))
        title = "%s - %s" % (event['EventName'],alliance['AllianceName'])
        playerlist = []
        info = myDB.select('SELECT PlayerID,PlayerName from players WHERE AllianceID = ? ORDER BY PlayerName ASC', (AllianceID,))
        for player in info:
            playerlist.append({'PlayerID': player['PlayerID'],
                               'PlayerName': player['PlayerName'],
                               })
        otherinfo = myDB.select('SELECT PlayerID,PlayerName,AllianceName,players.AllianceID from players,alliances WHERE players.AllianceID = \
                        alliances.AllianceID and players.AllianceID != ? ORDER BY PlayerName ASC', (AllianceID,))
        for player in otherinfo:
            if player['AllianceID'] > -1:
                displayname = '%s [%s]' % (player['PlayerName'],player['AllianceName'])
                playerlist.append({'PlayerID': player['PlayerID'],
                                   'PlayerName': displayname,})
        outsideinfo = myDB.select('SELECT PlayerID,PlayerName,AllianceName from players,alliances WHERE players.AllianceID = alliances.AllianceID and players.AllianceID = -1 ORDER BY PlayerName ASC')
        for player in outsideinfo:
            displayname = '%s [%s]' % (player['PlayerName'], player['AllianceName'])
            playerlist.append({'PlayerID': player['PlayerID'],
                               'PlayerName': displayname,})
        if len(eventplan) == 0:
            eventplan = None
            totalcommitments = 0
            plannedcommitments = 0
        else:
            commitments = myDB.select('SELECT PlayerID, CommittedPoints from eventcommitments WHERE EventID=? and AllianceID=?', (EventID, AllianceID,))
            newroster = dbactions.getEventRoster(EventID, AllianceID)
            plannedcommitments = newroster['TotalCommitment']
            totalcommitments = getTotal(commitments,'CommittedPoints')
        prioreventlist = dbactions.getEventList(event['EventType'], ByName=True)
        # logger.debug(playerlist)
        return serve_template("allianceevent.html", title=title, allianceevent=allianceevent, alliance=alliance, event=event, playerlist=playerlist, eventplan=eventplan, totalcommitments=totalcommitments, plannedcommitments=plannedcommitments, prioreventlist=prioreventlist)

    @cherrypy.expose
    @requireAuth()
    def eventPage(self, EventID, msg=None, tag=None):
        myDB=database.DBConnection()
        event = myDB.match("SELECT * from events WHERE EventID=?", (EventID,))
        eventplan = len(myDB.select("SELECT * from eventplanning WHERE EventID=?", (EventID,)))
        globaleventplan = myDB.match("SELECT * from eventplanning WHERE AllianceID=-3 and EventID=?", (EventID,))

        alliancelist = myDB.action('SELECT AllianceID,AllianceName from alliances WHERE AllianceID > -1 ORDER BY AllianceName ASC')
        title = '%s [%s]' %(event['EventName'],event['EventStartTime'])
        rosters = []
        if eventplan > 0:
            alliances = myDB.select("SELECT AllianceID from eventplanning where EventID=?", (EventID,))
            if len(alliances) > 0:
                for alliance in alliances:
                    rosters.append(dbactions.getEventRoster(EventID, alliance['AllianceID']))
        if len(globaleventplan) == 0:
            globaleventplan = None
        playerlist = []
        info = myDB.select('SELECT PlayerID,PlayerName from players WHERE AllianceID >= -1 ORDER BY PlayerName ASC')
        for player in info:
            playerlist.append({'PlayerID': player['PlayerID'],
                               'PlayerName': player['PlayerName'],
                               })

        return serve_template("event.html", title=title, event=event, alliancelist=alliancelist, msg=msg, tag=tag, eventplan=eventplan, rosters=rosters, playerlist=playerlist, globaleventplan=globaleventplan)


    # EVENT PLANNING ######
    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def eventPlanning(self, EventID, AllianceID):
        myDB = database.DBConnection()


    @cherrypy.expose
    def commitment(self, PlanID=None, msg=None, playerandalliance=-1, confirm=False):
        if not PlanID:
            return serve_template("public_message.html", title="Form Not Found", type='warning', msg="Sorry, the form ID seems to be invalid.  Please make sure the link you selected is correct.")
        myDB = database.DBConnection()
        plan = myDB.match("SELECT * from eventplanning where PlanUUID=?", (PlanID,))
        if len(plan) == 0:
            return serve_template("public_message.html", title="Form Not Found", type='warning',
                                  msg="Sorry, the form ID seems to be invalid.  Please make sure the link you selected is correct.")
        alliance = myDB.match("SELECT * from alliances where AllianceID=?", (plan['AllianceID'],))
        event = myDB.match("SELECT * from events where EventID=?", (plan['EventID'],))
        starttime = datetime.datetime.strptime(event['EventStartTime'],'%Y-%m-%dT%H:%M')
        if datetime.datetime.now() >= starttime:
            return serve_template("public_message.html", title="Event Already Started", type='warning', msg="Sorry, you cannot change your \
                                                                                          commitment after the event has begun.")
        playerlist = []
        if plan['AllianceID'] >= 0:
            playerdata = myDB.select("SELECT PlayerID, PlayerName, players.AllianceID, alliances.AllianceName from players, alliances where players.AllianceID=alliances.AllianceID and players.AllianceID=? ORDER BY PlayerName ASC", (plan['AllianceID'],))
            alliancename = alliance['AllianceName']
        else:
            playerdata = myDB.select("SELECT PlayerID, PlayerName, players.AllianceID, alliances.AllianceName from players, alliances where players.AllianceID=alliances.AllianceID and players.AllianceID>=0 ORDER BY PlayerName ASC")
            alliancename = "Global"
        for player in playerdata:
            playerlist.append({'playerandalliance': '%s|%s' % (player['PlayerID'], player['AllianceID']), 'PlayerName': '%s (%s)' % (player['PlayerName'], player['AllianceName'])})
        title = "%s - %s - Commitment Form" % (event['EventName'], alliancename)
        return serve_template("commitmentform.html", title=title, playerlist=playerlist, alliance=alliance, event=event, minimum=plan['AllianceMinimumPoints'], msg=msg, playerandalliance=playerandalliance, confirm=confirm, PlanID=PlanID)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def officerCommitmentUpdate(self, playerid='', eventid='', commitment='', availfriday=False, availsaturday=False, availsunday=False, willingtomove=False, notes=''):
        self.label_thread()
        _session=get_session_info()
        myDB = database.DBConnection()
        if playerid and eventid:
            player = myDB.match("SELECT * from players where PlayerID=?", (playerid,))
            alliance = myDB.match("SELECT * from alliances where AllianceID=?", (player['AllianceID'],))
            event = myDB.match("SELECT * from events where EventID=?", (eventid,))
            availfriday = checkToInt(availfriday)
            availsaturday = checkToInt(availsaturday)
            availsunday = checkToInt(availsunday)
            willingtomove = checkToInt(willingtomove)

            controlValueDict = {'PlayerID': playerid,
                                'EventID': eventid,
                                'AllianceID': player['AllianceID']
                                }
            try:
                commitment = int(commitment)
            except:
                commitment = 0
            newValueDict = {
                'CommittedPoints': commitment,
                'WillingToMove': int(willingtomove),
                'AvailFriday': int(availfriday),
                'AvailSaturday': int(availsaturday),
                'AvailSunday': int(availsunday),
                'Notes': notes,
            }
            myDB.upsert('eventcommitments',newValueDict,controlValueDict)
            dblogger.player(
                'Added commitment: %s in %s [%s points]' % (event['EventName'], alliance['AllianceName'], commitment,),
                _session['user'], playerid)
            dblogger.alliance(
                'Added commitment for %s: %s [%s points]' % (event['EventName'], player['PlayerName'], commitment,),
                _session['user'], alliance['AllianceID'])
            msg = "Set %s's commitment to %s" % (player['PlayerName'], commitment)
            return self.eventPage(EventID=eventid, msg=msg, tag="movement")

    @cherrypy.expose
    def commitmentUpdate(self, playerandalliance='', eventid='', confirmplayerupdate=False, commitment ='', availfriday=False, availsaturday=False, availsunday=False, willingtomove=False, notes='', planid=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        # logger.debug('PlayerID: %s' % playerid)
        if playerandalliance and eventid:
            playerid, allianceid = playerandalliance.split('|')
            player = myDB.match("SELECT * from players where PlayerID = ?", (playerid,))
            alliance = myDB.match("SELECT * from alliances where AllianceID = ?", (allianceid,))
            event = myDB.match("SELECT * from events where EventID = ?", (eventid,))
            prevcommit = myDB.match("SELECT * from eventcommitments where PlayerID = ? and AllianceID = ? and EventID = ?", (playerid, allianceid, eventid,))
            remote_host = cherrypy.request.remote.ip
            confirmplayerupdate = checkToBool(confirmplayerupdate)
            availfriday = checkToInt(availfriday)
            availsaturday = checkToInt(availsaturday)
            availsunday = checkToInt(availsunday)
            willingtomove = checkToInt(willingtomove)

            # logger.debug('%s - %s - %s - %s - %s' % (confirmplayerupdate, willingtomove, availfriday, availsaturday, availsunday))
            if len(prevcommit) > 0 and confirmplayerupdate == False:
                if planid:
                    msg = "This player has already submitted a commitment.  If you are updating your commitment, please check the box below the Player Name"
                    return self.commitment(PlanID=planid, msg=msg, playerandalliance=playerandalliance)
                else:
                    return serve_template("public_message.html", title="Something went wrong", type="danger", msg="Something went wrong.  Please try the commitment link again. (ERR_NO_PLANID)")
            else:
                if not isinstance(commitment, (int, long)):
                    try:
                        commitment = int(commitment)
                    except:
                        msg = "Invalid commitment value: '%s'" % commitment
                        return self.commitment(PlanID=planid, msg=msg, PlayerID=playerid)
                controlValueDict = {'PlayerID': playerid,
                                    'EventID': eventid,
                                    'AllianceID': allianceid}
                newValueDict = {
                    'CommittedPoints': commitment,
                    'WillingToMove': int(willingtomove),
                    'AvailFriday': int(availfriday),
                    'AvailSaturday': int(availsaturday),
                    'AvailSunday': int(availsunday),
                    'Notes': notes,
                }
                myDB.upsert('eventcommitments',newValueDict,controlValueDict)
                if len(prevcommit) > 0:
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(prevcommit[key]):
                            dblogger.player('Commitment for %s in %s [%s] changed from %s to %s' % (event['EventName'], alliance['AllianceName'], key, prevcommit[key], newValueDict[key]), remote_host, playerid)
                            dblogger.alliance('Commitment for %s [%s] edited [%s] from %s to %s' % (event['EventName'], player['PlayerName'], key, prevcommit[key], newValueDict[key]), remote_host, allianceid)
                else:
                    commitmentcreated = myDB.match('SELECT * from eventcommitments WHERE EventID=? and PlayerID=? and AllianceID = ?', (eventid, playerid, allianceid,))
                    if not len(commitmentcreated):
                        return serve_template("public_message.html", title="Something went wrong", type="danger", msg="Something went wrong.  Please try the commitment link again. (ERR_NO_COMMITEMNT_CREATED)")
                    else:
                        dblogger.player('Added commitment: %s in %s [%s points]' % (event['EventName'], alliance['AllianceName'], commitment,), remote_host, playerid)
                        dblogger.alliance('Added commitment for %s: %s [%s points]' % (event['EventName'], player['PlayerName'], commitment,), remote_host, allianceid)
            return serve_template("public_message.html", title="Commitment Submitted", type="info", msg="Thank you for your commitment, %s" % (player['PlayerName']))


    @cherrypy.expose
    def getEventPlanningAll(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="",
                        **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        # cmd = 'SELECT players.playerID, players.PlayerName, players.AllianceID, alliances.AllianceName, eventcommitments.CommittedPoints, events.EventName, AvailFriday, AvailSaturday, AvailSunday, WillingToMove, Notes FROM players,alliances,events NATURAL JOIN eventcommitments WHERE players.AllianceID = alliances.AllianceID'
        cmd = 'SELECT players.playerID, players.PlayerName, players.AllianceID, alliances.AllianceName, events.EventID, EventName, CommittedPoints, AvailFriday, AvailSaturday, AvailSunday, WillingToMove, Notes, NewAllianceID FROM players,alliances,events NATURAL LEFT JOIN eventcommitments NATURAL LEFT JOIN eventplanningmovements WHERE players.AllianceID = alliances.AllianceID and players.AllianceID >= -1'
        args = []
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                if int(row['EventID']) == int(kwargs['EventID']):
                    temprow = []
                    temprow.append('%s|%s' % (row['PlayerID'], row['EventID']))
                    temprow.append(row['PlayerName'])
                    temprow.append(row['AllianceName'])
                    temprow.append(row['CommittedPoints'])
                    if row['NewAllianceID']:
                        newAlliance = getAlliance(row['NewAllianceID'])
                        temprow.append(newAlliance['AllianceName'])
                    else:
                        temprow.append(row['AllianceName'])
                    temprow.append(intToYesNo(row['AvailFriday']))
                    temprow.append(intToYesNo(row['AvailSaturday']))
                    temprow.append(intToYesNo(row['AvailSunday']))
                    temprow.append(intToYesNo(row['WillingToMove']))
                    temprow.append(row['Notes'])
                    rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        # logger.debug(mydict)
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    def getEventPlanning(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="",
                        **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT eventcommitments.PlayerID, players.PlayerName, eventcommitments.CommittedPoints, eventcommitments.EventID, events.EventName, events.EventStartTime, eventcommitments.AllianceID, alliances.AllianceName, AvailFriday, AvailSaturday, AvailSunday, WillingToMove, Notes FROM eventcommitments,players,alliances,events WHERE eventcommitments.PlayerID = players.PlayerID and eventcommitments.AllianceID = alliances.AllianceID and eventcommitments.EventID = events.EventID'
        args = []
        if kwargs['source'] == "Alliance":
            cmd += ' and eventcommitments.AllianceID = ? and eventcommitments.EventID = ?'
            args.append(kwargs['AllianceID'])
            args.append(kwargs['EventID'])
        elif kwargs['source'] == "Player":
            cmd += ' and eventcommitments.PlayerID = ?'
            args.append(kwargs['PlayerID'])
        elif kwargs['source'] == "All":
            cmd += ' and eventcommitments.EventID = ?'
            args.append(kwargs['EventID'])
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                temprow = []
                if kwargs['source'] == "Alliance":
                    temprow.append(row['PlayerName'])
                    temprow.append(row['CommittedPoints'])
                    temprow.append(intToYesNo(row['AvailFriday']))
                    temprow.append(intToYesNo(row['AvailSaturday']))
                    temprow.append(intToYesNo(row['AvailSunday']))
                    temprow.append(intToYesNo(row['WillingToMove']))
                    temprow.append(row['Notes'])
                    rows.append(temprow)
                elif kwargs['source'] == "Player":
                    temprow.append(row['EventStartTime'])
                    temprow.append(row['EventName'])
                    temprow.append(row['AllianceName'])
                    temprow.append(row['CommittedPoints'])
                    temprow.append(intToYesNo(row['AvailFriday']))
                    temprow.append(intToYesNo(row['AvailSaturday']))
                    temprow.append(intToYesNo(row['AvailSunday']))
                    temprow.append(intToYesNo(row['WillingToMove']))
                    temprow.append(row['Notes'])
                    rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        # logger.debug(mydict)
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def eventMovement(self, EventID=None, redirect='', destination='', **args):
        _session = get_session_info()
        logger.debug(args)
        myDB = database.DBConnection()
        movedplayers = []
        for pid in args:
            if not pid == 'eventplanning_table_length':
                ids = pid.split('|')
                player = myDB.match('SELECT * from players WHERE PlayerID=?', (ids[0],))
                event = myDB.match('SELECT * from events WHERE EventID=?', (ids[1],))
                oldalliance = myDB.match('SELECT * from alliances WHERE AllianceID=?', (player['AllianceID'],))
                alliance = myDB.match('SELECT * from alliances where AllianceID=?', (destination,))
                if oldalliance['AllianceID'] != alliance['AllianceID']:
                    myDB.upsert("eventplanningmovements", {'NewAllianceID': destination},{'PlayerID': ids[0], 'EventID': ids[1]})
                    movedplayers.append(player['PlayerName'])
                else:
                    myDB.action('DELETE from eventplanningmovements WHERE PlayerID=? and EventID = ?', (ids[0], ids[1],))
                    movedplayers.append(player['PlayerName'])
        if len(movedplayers) > 0:
            dbactions.updateAlliancePlayers(alliance['AllianceID'])
            dbactions.updateAlliancePlayers(oldalliance['AllianceID'])
            msg = "Relocated %s to %s" % (movedplayers, alliance['AllianceName'])
        else:
            msg = "No Players moved."
        if redirect=='event':
                return self.eventPage(EventID=ids[1], tag='movement', msg=msg)


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def eventFunctions(self, EventID=None, function='', redirect=''):
        msg = ''
        if function == 'Refresh':
            dbactions.updateAllianceEvents(EventID)
            msg = 'Event updated'
        elif function == 'Planning':
            dbactions.updateAllianceEventPlanning(EventID)
            msg = 'Planning sessions created'
        if redirect == 'Event':
            return self.eventPage(EventID=EventID, msg=msg)

    @cherrypy.expose
    def getMovement(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="",
                        **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT PlayerID, PlayerName, players.AllianceID, NewAllianceID from players NATURAL JOIN eventplanningmovements WHERE eventplanningmovements.EventID = ?"
        args = [kwargs['EventID']]
        if kwargs['Type'] == 'Out':
            cmd += " and AllianceID = ?"
            args.append(kwargs['AllianceID'])
        elif kwargs['Type'] == 'In':
            cmd += " and NewAllianceID = ?"
            args.append(kwargs['AllianceID'])

        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                temprow = []
                oldAlliance = getAlliance(row['AllianceID'])
                newAlliance = getAlliance(row['NewAllianceID'])
                temprow.append(oldAlliance['AllianceName'])
                temprow.append(row['PlayerName'])
                temprow.append(dbactions.getCommitment(PlayerID=row['PlayerID'], EventID=kwargs['EventID'],Total=True))
                temprow.append(newAlliance['AllianceName'])
                rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s


    @cherrypy.expose
    def getEventRoster(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="",
                        **kwargs):
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        rows = []
        filtered = []
        if kwargs['AllianceID'] and kwargs['EventID']:
            allianceroster = dbactions.getEventRoster(kwargs['EventID'], kwargs['AllianceID'])
        else:
            allianceroster = {'Roster': [], 'AllianceName': '', 'TotalCommitment': 0}
        if len(allianceroster['Roster']):
            for row in allianceroster['Roster']:
                temprow = []
                temprow.append(row['PlayerName'])
                temprow.append(row['CommittedPoints'])
                temprow.append(row['AvailFriday'])
                temprow.append(row['AvailSaturday'])
                temprow.append(row['AvailSunday'])
                temprow.append(row['WillingToMove'])
                temprow.append(row['Notes'])
                rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(allianceroster['Roster']),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    # WARS ######
    @cherrypy.expose
    @requireAuth()
    def wars(self):
        title = "Wars"
        return serve_template(templatename="wars.html", title=title)

    @cherrypy.expose
    @requireAuth()
    def getWars(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT WarID, WarStartTime, AllianceName, WarOpponent, wars.WarLeague, TotalWarScore, OpponentWarScore from wars,alliances '
        cmd += 'WHERE wars.AllianceID = alliances.AllianceID'

        args = []
        if kwargs['source'] == "Alliance":
            cmd += ' and wars.AllianceID=?'
            args.append(kwargs['AllianceID'])

        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                rows.append(list(row))

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth()
    def getWarStatistics(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT WarID, WarStartTime, WarOpponent, WarLeague, TotalWarScore, OpponentWarScore from wars '
        cmd += 'WHERE AllianceID=?'
        args = [kwargs['AllianceID']]
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                wardetails = dbactions.getWarDetails(row['WarID'])
                temprow = []
                temprow.append(row['WarStartTime'])
                temprow.append(row['WarOpponent'])
                temprow.append(row['WarLeague'])
                temprow.append(wardetails['participants'])
                index = 0
                topscorers = "%s: " % wardetails['topscore']
                for score in wardetails['topscorers']:
                    if index > 0:
                        topscorers += ", "
                    topscorers += score['PlayerName']
                    index += 1
                temprow.append(topscorers)
                index = 0
                noshows = ""
                for score in wardetails['noshows']:
                    if index > 0:
                        noshows += ", "
                    noshows += score['PlayerName']
                    index += 1
                temprow.append(noshows)
                temprow.append(row['WarID'])
                rows.append(temprow)

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s


    @cherrypy.expose
    def getWarScores(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT warscores.PlayerID, players.PlayerName, WarScore, WarAttemptsLeft, AllianceName, WarOpponent, WarStartTime, warscores.WarID, wars.WarLeague FROM warscores,players,wars,alliances WHERE warscores.PlayerID = players.PlayerID and warscores.WarID=wars.WarID and wars.AllianceID=alliances.AllianceID'
        args = []
        if kwargs['source'] == "War":
            cmd += ' and warscores.WarID = ?'
            args.append(kwargs['WarID'])
        elif kwargs['source'] == "Player":
            cmd += ' and warscores.PlayerID = ?'
            args.append(kwargs['PlayerID'])
        # logger.debug("Query: %s -- Args: %s" % (cmd,args))
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                if kwargs['source'] == "Player":
                    temprow = list(row)
                    newtemprow = []
                    newtemprow.append(temprow[6])
                    newtemprow.append("%s vs %s" % (temprow[4],temprow[5]))
                    newtemprow.append(temprow[8])
                    newtemprow.append(temprow[2])
                    newtemprow.append(temprow[3])
                    newtemprow.append(temprow[7])
                    rows.append(newtemprow)
                else:
                    rows.append(list(row))

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        # logger.debug(mydict)
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    def getWarSummary(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        rows = []
        filtered = []
        summaryCount = alliancekeeper.CONFIG['WAR_SUMMARY_COLUMNS']
        if 'AllianceID' in kwargs:
            cmd = 'SELECT * from Players WHERE AllianceID=?'
            args = [kwargs['AllianceID']]
            rowlist = myDB.select(cmd, tuple(args))
            if len(rowlist):
                for row in rowlist:
                    recentWars = dbactions.getPlayerRecentWars(row['PlayerID'], summaryCount)
                    temprow = []
                    temprow.append('<a href="/playerPage?PlayerID=%s">%s</a>' % (row['PlayerID'], row['PlayerName']))
                    for i in range(0,summaryCount):

                        if i < len(recentWars):
                            temprow.append('%s/%s' % (recentWars[i]['WarScore'], recentWars[i]['WarAttemptsLeft']))
                        else:
                            temprow.append('')
                    rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        # logger.debug(mydict)
        s = simplejson.dumps(mydict)
        return s


    @cherrypy.expose
    @requireAuth()
    def warPage(self, WarID, ShowAllPlayers=0):
        myDB = database.DBConnection()
        war = myDB.match("SELECT * from wars WHERE WarID=?", (WarID,))
        alliance = myDB.match("SELECT * from alliances WHERE AllianceID=?", (war['AllianceID'],))
        wardetails = dbactions.getWarDetails(WarID)
        # logger.debug("War Deets: %s" % wardetails)
        title = "%s vs %s - %s" % (alliance['AllianceName'], war['WarOpponent'], war['WarStartTime'])
        timestamp=currentTime()
        playerlist = []
        info = myDB.select('SELECT PlayerID,PlayerName from players WHERE AllianceID = ? ORDER BY PlayerName ASC', (war['AllianceID'],))
        for player in info:
            playerlist.append({'PlayerID': player['PlayerID'],
                               'PlayerName': player['PlayerName'],
                               })
        if int(ShowAllPlayers):
            otherinfo = myDB.select('SELECT PlayerID,PlayerName,AllianceName,players.AllianceID from players,alliances WHERE players.AllianceID = \
                            alliances.AllianceID and players.AllianceID != ? ORDER BY PlayerName ASC', (war['AllianceID'],))
            for player in otherinfo:
                if player['AllianceID'] > -1:
                    displayname = '%s [%s]' % (player['PlayerName'], player['AllianceName'])
                    playerlist.append({'PlayerID': player['PlayerID'],
                                       'PlayerName': displayname, })
            outsideinfo = myDB.select(
                'SELECT PlayerID,PlayerName,AllianceName from players,alliances WHERE players.AllianceID = alliances.AllianceID and players.AllianceID = -1 ORDER BY PlayerName ASC')
            for player in outsideinfo:
                displayname = '%s [%s]' % (player['PlayerName'], player['AllianceName'])
                playerlist.append({'PlayerID': player['PlayerID'],
                                   'PlayerName': displayname, })
        return serve_template(
            templatename="war.html", title=title, war=war, alliance=alliance, timestamp=timestamp, playerlist=playerlist, showallplayers=ShowAllPlayers)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def editWar(self, WarID=None, AllianceID=None, msg=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from wars WHERE WarID=?', (WarID,))
        if data:
            response = data
            alliance = myDB.match('SELECT * from alliances where AllianceID=?', (data['AllianceID'],))
        else:
            alliance = myDB.match('SELECT * from alliances where AllianceID=?', (AllianceID,))
            response = {
                'WarID': -1,
                'TimeStamp': currentTime(),
                'WarStartTime': currentTime(),
                'WarOpponent': "",
                'TotalWarScore': 0,
                'OpponentWarScore': 0,
                'AllianceID': alliance['AllianceID'],
                'WarLeague': alliance['WarLeague'],
            }

        _session = get_session_info()
        if response['WarID'] == -1:
            title = "Create War"
        else:
            title = "Edit War"
        return serve_template(templatename="editwar.html", title=title, war=response, msg=msg, alliance=alliance)

    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def warUpdate(self, warid='', allianceid='', waropponent='', timestamp='', warstarttime ='', totalwarscore='', opponentwarscore='', warleague=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if warid:
            if int(warid) <= -1:
                warid = None
                data = None
            else:
                data = myDB.match('SELECT * from wars where WarID=?', (warid,))
            if waropponent != '':
                controlValueDict = {'WarID': warid}
                newValueDict = {
                    'AllianceID': allianceid,
                    'WarOpponent': waropponent,
                    'TimeStamp': timestamp,
                    'WarStartTime': warstarttime,
                    'TotalWarScore': totalwarscore,
                    'OpponentWarScore': opponentwarscore,
                    'WarLeague': warleague,
                }
                myDB.upsert('wars',newValueDict,controlValueDict)
                alliance = myDB.match("SELECT * from alliances WHERE AllianceID=?", (allianceid,))
                if data:
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            dblogger.user('War modified %s vs %s [%s] from %s to %s' % (data['WarOpponent'], alliance['AllianceName'], key, data[key], newValueDict[key]), _session['user'], _session['user_id'])
                            dblogger.alliance('War vs %s edited [%s] from %s to %s' % (data['WarOpponent'], key, data[key], newValueDict[key]), _session['user'], allianceid)
                    return self.warPage(WarID=data['WarID'])
                else:
                    warcreated = myDB.match('SELECT * from wars WHERE WarOpponent=? and TimeStamp=?', (waropponent, timestamp,))
                    if not len(warcreated):
                        return self.editWar(WarID=warid, msg="Something went wrong.  War not created.")
                    else:
                        dblogger.user('Created war: %s vs %s' % (alliance['AllianceName'], waropponent),_session['user'], _session['user_id'])
                        dblogger.alliance('Created war: vs %s' % (waropponent), _session['user'], allianceid)
                        return self.warPage(WarID=warcreated['WarID'])
            raise cherrypy.HTTPRedirect("alliancePage?AllianceID=%s" % (allianceid))


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def warscoresUpdate(self, warid='', playerid='', warscore='', timestamp='', warattemptsleft='', showallplayers=0):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if warid:
            data = myDB.match('SELECT * from warscores where WarID=? and PlayerID=?', (warid,playerid))
            if warscore != '':
                controlValueDict = {'WarID': warid,
                                    'PlayerID': playerid}
                newValueDict = {
                    'WarScore': warscore,
                    'WarAttemptsLeft': warattemptsleft,
                    'TimeStamp': timestamp,
                }
                myDB.upsert('warscores', newValueDict, controlValueDict)
                war = myDB.match("SELECT * from wars where WarID=?", (warid,))
                # logger.debug("War: %s - Alliance: %s" % (war['WarID'], war['AllianceID']) )
                alliance = myDB.match("SELECT * from alliances WHERE AllianceID=?", (war['AllianceID'],))
                player = myDB.match("SELECT * from players WHERE PlayerID=?", (playerid,))
                if len(data) > 0:
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            dblogger.user('War Score modified %s vs %s: %s [%s] from %s to %s' % (
                            alliance['AllianceName'], war['WarOpponent'], player['PlayerName'], key, data[key], newValueDict[key]),
                                          _session['user'], _session['user_id'])
                            dblogger.alliance('War Score vs %s edited: %s [%s] from %s to %s' % (
                                war['WarOpponent'], player['PlayerName'],key, data[key], newValueDict[key]),
                                _session['user'], alliance['AllianceID'])
                            dblogger.player('War Score vs %s edited: [%s] from %s to %s' % (war['WarOpponent'],
                                key, data[key], newValueDict[key]), _session['user'], playerid)
                else:
                    scorecreated = myDB.match("Select * from warscores where WarID=? and PlayerID=?", (warid, playerid))
                    if not len(scorecreated):
                        return self.warPage(WarID=warid, msg="Something went wrong.  Score not added.")
                    else:
                        dblogger.user('Created war score: %s vs %s: %s [%s with %s attempts left]' % (alliance['AllianceName'], war['WarOpponent'], player['PlayerName'], warscore, warattemptsleft),
                                      _session['user'], _session['user_id'])
                        dblogger.alliance('Created war score: vs %s: %s [%s with %s attempts left]' % (war['WarOpponent'], player['PlayerName'], warscore, warattemptsleft), _session['user'], alliance['AllianceID'])
                        dblogger.player('Created war score: %s vs %s: [%s with %s attempts left]' % (
                            alliance['AllianceName'], war['WarOpponent'], warscore, warattemptsleft), _session['user'], playerid)
            dbactions.updateAllianceWar(warid)
            dbactions.updatePlayer(playerid)
            return self.warPage(WarID=warid, ShowAllPlayers=showallplayers)


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def warActions(self, warAction=None, warClass=None, war_table_length="10", **kwargs):
        wars = []
        for key in kwargs.keys():
            if str(kwargs[key]) == "on":
                wars.append(int(key))
        logger.debug("%s - %s - %s" % (warAction, warClass, wars))
        return serve_template("public_message.html", title="Coming Soon - %s" % warAction, type='warning',msg=wars)


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def downloadWarDetail(self, AllianceID=None, start=None, end=None):
        if AllianceID:
            if start and end:
                success, filename, fileloc, msg = dbactions.exportWarDetails(AllianceID=AllianceID, starttime=start, endtime=end)
                if success:
                    return serve_download(fileloc, filename)
                else:
                    return self.alliancePage(AllianceID=AllianceID, msg=msg)
            else:
                return self.alliancePage(AllianceID=AllianceID, msg="Invalid Start or End Date")
        else:
            return self.home(msg="Could not find Alliance")

    # BASE/GUN INDEX ######

    @cherrypy.expose
    @requireAuth()
    def baseindex(self):
        title = "Base/Gun Index"
        return serve_template(templatename="baseindexes.html", title=title)


    @cherrypy.expose
    @requireAuth()
    def editBaseIndex(self, PlayerID=None, IndexID=None, msg=None, HQ=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from gunmatrix WHERE IndexID=?', (IndexID,))
        previousindex = None
        if data:
            response = data
            player = getPlayer(data['PlayerID'])
        else:
            player = getPlayer(PlayerID)
            logger.debug(player)
            response = {
                'IndexID': -1,
                'TimeStamp': currentTime(),
                'IndexDate': currentTime(),
                'HQLevel': player['HeadQuarters'],
                'MedalCount': player['MedalCount'],
                'PlayerLevel': player['PlayerLevel'],
                'PlayerID': player['PlayerID'],
                'BuildBots': 2,
            }
            returnhq = 0
            if HQ:
                returnhq = HQ
            else:
                returnhq = player['HeadQuarters']
            previousindex = myDB.match('SELECT * from gunmatrix WHERE PlayerID=? ORDER BY IndexDate DESC', (PlayerID,))
            if len(previousindex) == 0:
                previousindex = None
        logger.debug('Previous Index: %s' % previousindex)
        for key in previousindex.keys():
            logger.debug('Key: %s - Value: %s' % (key, previousindex[key]))
        _session = get_session_info()
        if response['IndexID'] == -1:
            title = "Create Base Index"
        else:
            title = "Edit Base Index"
        return serve_template(templatename="editindex.html", title=title, baseindex=response, msg=msg, player=player, previousindex=previousindex, hq=returnhq)


    @cherrypy.expose
    @requireAuth(min_access_level(1))
    def indexUpdate(self, PlayerID='', IndexID='', TimeStamp='', IndexDate='', **kwargs):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        logger.debug(kwargs)
        if IndexID:
            if int(IndexID) <= -1:
                IndexID = None
                data = None
            else:
                data = myDB.match('SELECT * from gunmatrix where IndexID=?', (IndexID,))
            if PlayerID != '':
                player = getPlayer(PlayerID)
                controlValueDict = {'IndexID': IndexID,
                                    'PlayerID': PlayerID,}
                newValueDict = {'TimeStamp': TimeStamp,
                                'IndexDate': IndexDate}
                for key in kwargs.keys():
                    newValueDict[key] = stringToInt(kwargs[key])
                logger.debug(newValueDict)

                myDB.upsert('gunmatrix',newValueDict,controlValueDict)
                if data:
                    for key, value in newValueDict.iteritems():
                        if uni(newValueDict[key]) != uni(data[key]):
                            dblogger.user('Index %s - %s [%s] from %s to %s' % (player['PlayerName'], data['IndexDate'], key, data[key], newValueDict[key]), _session['user'], _session['user_id'])
                            dblogger.player('Index %s edited [%s] from %s to %s' % (data['IndexDate'], key, data[key], newValueDict[key]), _session['user'], PlayerID)
                else:
                    indexcreated = myDB.match('SELECT * from gunmatrix WHERE PlayerID=? and TimeStamp=?', (PlayerID, TimeStamp,))
                    if not len(indexcreated):
                        return self.editBaseIndex(IndexID=IndexID, msg="Something went wrong.  Index not created.")
                    else:
                        dblogger.user('Created Base Index: %s - %s' % (player['PlayerName'], IndexDate.encode('ascii')),_session['user'], _session['user_id'])
                        dblogger.player('Index %s created' % (IndexDate), _session['user'], PlayerID)
            dbactions.updatePlayer(PlayerID)
            raise cherrypy.HTTPRedirect("playerPage?PlayerID=%s#basegunindex" % (PlayerID))


    @cherrypy.expose
    def getBaseIndex(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="",
                    **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        alliancekeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT players.PlayerID, PlayerName, IndexID, IndexDate, players.AllianceID from gunmatrix,players,alliances WHERE gunmatrix.PlayerID = players.PlayerID and players.AllianceID = alliances.AllianceID'
        args = []
        if kwargs['source'] == "Player":
            cmd += ' and gunmatrix.PlayerID=?'
            args.append(kwargs['PlayerID'])
        elif kwargs['source'] == "Alliance":
            cmd += ' and players.AllianceID=?'
            args.append(kwargs['AllianceID'])
        cmd += ' ORDER BY IndexDate DESC'
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                temprow = []
                index = dbactions.getBaseIndex(row['IndexID'])
                temprow.append(index['IndexDate'])
                if kwargs['source'] == "Alliance" or kwargs['source'] == "All":
                    temprow.append(index['PlayerName'])
                if kwargs['source'] == "All":
                    temprow.append(index['AllianceName'])
                temprow.append(index['HQLevel'])
                temprow.append(index['BuildBots'])
                temprow.append(index['MedalCount'])
                temprow.append(index['PlayerLevel'])
                for group in index['DefenseGroups']:
                    temprow.append(group)
                temprow.append(index['WallGroup'])
                temprow.append(index['PlayerID'])
                temprow.append(index['AllianceID'])
                rows.append(temprow)
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    def getTempInfo(self, AllianceID=None):
        return str(dbactions.getEventParticipation(AllianceID=AllianceID))

    # ACTIVITY ######

    @cherrypy.expose
    def getActivity(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        cmd = ''
        args=[]
        if kwargs['source'] == "Player":
            cmd = 'SELECT TimeStamp, ActivityDate, ActivityDesc, UserName from playeractivity WHERE PlayerID=?'
            args.append(kwargs['PlayerID'])
        elif kwargs['source'] == "Alliance":
            cmd = 'SELECT TimeStamp, ActivityDate, ActivityDesc, UserName from allianceactivity WHERE AllianceID=?'
            args.append(kwargs['AllianceID'])
        elif kwargs['source'] == "User":
            cmd = 'SELECT TimeStamp, ActivityDate, ActivityDesc, UserName from useractivity WHERE UserID=?'
            args.append(kwargs['UserID'])
        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        if len(rowlist):
            for row in rowlist:
                rows.append(list(row))
            if sSearch:
                filtered = filter(lambda x: sSearch.lower in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s


    # SEARCH #######

    @cherrypy.expose
    @requireAuth()
    def search(self, name):
        if name is None or not name:
            raise cherrypy.HTTPRedirect("home")

        myDB = database.DBConnection()
        resultlist = []
        alliancesearch = myDB.select("SELECT AllianceID,AllianceName from alliances")
        for item in alliancesearch:
            if name.lower() in item['AllianceName'].lower():
                resultlist.append({'Type': 'Alliance', 'Name': item['AllianceName'], 'URL': 'alliancePage?AllianceID=%s' % item['AllianceID']})

        playersearch = myDB.select("SELECT PlayerID,PlayerName from players")
        for item in playersearch:
            if name.lower() in item['PlayerName'].lower():
                resultlist.append({'Type': 'Player', 'Name': item['PlayerName'], 'URL': 'playerPage?PlayerID=%s' % item['PlayerID']})

        eventsearch = myDB.select("SELECT EventName,EventID,EventStartTime from events")
        for item in eventsearch:
            if name.lower() in item['EventName'].lower():
                resultlist.append(
                    {'Type': 'Event', 'Name': '%s - %s' % (item['EventName'], item['EventStartTime']), 'URL': 'eventPage?EventID=%s' % item['EventID']})

        warsearch = myDB.select("SELECT WarOpponent,AllianceID,WarID,WarStartTime from wars")
        for item in warsearch:
            if name.lower() in item['WarOpponent'].lower():
                alliance = myDB.match("SELECT AllianceName from alliances WHERE AllianceID=?", (item['AllianceID'],))
                resultlist.append({'Type': 'War', 'Name': '%s vs %s - %s' % (alliance['AllianceName'], item['WarOpponent'], item['WarStartTime']), 'URL': 'warPage?WarID=%s' % item['WarID']})

        # logger.debug(resultlist)
        return serve_template(templatename="searchresults.html", title='Search Results: "' + name + '"', resultlist=resultlist)


    # JOB CONTROL ######

    @cherrypy.expose
    @requireAuth(min_access_level(3))
    def shutdown(self):
        alliancekeeper.config_write()
        alliancekeeper.SIGNAL = 'shutdown'
        message = 'closing ... '
        return serve_template(templatename="shutdown.html", prefix='AllianceKeeper is ', title= "Close keeper", \
                                message=message, timer=15)

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def restart(self):
        alliancekeeper.SIGNAL = 'restart'
        message = 'reopening ... '
        return serve_template(templatename="shutdown.html", prefix='AllianceKeeper is ', title = "Reopen keeper",\
                              message=message, timer=30)

    # UPDATES ######
    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def checkForUpdates(self):
        self.label_thread()
        versioncheck.checkForUpdates()
        if alliancekeeper.CONFIG['COMMITS_BEHIND'] == 0:
            if alliancekeeper.COMMIT_LIST:
                message = "unknown status"
                messages = alliancekeeper.COMMIT_LIST.replace('\n', '<br>')
                message = message + '<br><small>' + messages
            else:
                message = "up to date"
            return serve_template(templatename="shutdown.html", prefix='LazyLibrarian is ',
                                  title="Version Check", message=message, timer=5)

        elif alliancekeeper.CONFIG['COMMITS_BEHIND'] > 0:
            message = "behind by %s commit%s" % (alliancekeeper.CONFIG['COMMITS_BEHIND'],
                                                 plural(alliancekeeper.CONFIG['COMMITS_BEHIND']))
            messages = alliancekeeper.COMMIT_LIST.replace('\n', '<br>')
            message = message + '<br><small>' + messages
            return serve_template(templatename="shutdown.html", title="Commits", prefix='AllianceKeeper is ',
                                  message=message, timer=15)

        else:
            message = "unknown version"
            messages = "Your version is not recognised at<br>https://github.com/%s/%s  Branch: %s" % (
                alliancekeeper.CONFIG['GIT_USER'], alliancekeeper.CONFIG['GIT_REPO'], alliancekeeper.CONFIG['GIT_BRANCH'])
            message = message + '<br><small>' + messages
            return serve_template(templatename="shutdown.html", title="Commits", prefix='LazyLibrarian is ',
                                  message=message, timer=15)

            # raise cherrypy.HTTPRedirect("config")

    @cherrypy.expose
    @requireAuth(min_access_level(2))
    def update(self):
        logger.debug('(webServe-Update) - Performing update')
        alliancekeeper.SIGNAL = 'update'
        message = 'Updating...'
        return serve_template(templatename="shutdown.html", prefix='AllianceKeeper is ', title="Updating",
                              message=message, timer=30)
