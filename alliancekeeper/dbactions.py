import uuid
import os
import unicodecsv as csv

import alliancekeeper
import lib.simplejson as simplejson

from alliancekeeper import database, logger
from alliancekeeper.common import intToYesNo, uni, todayMinusDays

def updateAllianceEvents(EventID):
    myDB = database.DBConnection()
    event = myDB.match("SELECT * from events where EventID=?", (EventID,))
    alliances = myDB.select("SELECT * from alliances")
    totalScore = 0
    for alliance in alliances:
        if alliance['AllianceID'] > -1:
            allianceevent = myDB.match("SELECT * from allianceevents WHERE AllianceID=? and EventID=?", (alliance['AllianceID'], EventID))
            if len(allianceevent) == 0:
                controlValueDict = {'AllianceID': alliance['AllianceID'],
                                    'EventID': EventID,}
                newValueDict = {'AlliancePoints': 0,
                                'TimeCompleted': "",}
                logger.debug('Creating alliance event: %s [%s]' % (event['EventName'],alliance['AllianceName']))
                myDB.upsert('allianceevents',newValueDict, controlValueDict)
                allianceevent = myDB.match("SELECT * from allianceevents WHERE AllianceID=? and EventID=?",
                                           (alliance['AllianceID'], EventID))
            tempScore = 0
            if event['EventType'] == 'Leaderboard':
                eventscores = myDB.select("SELECT * from eventresults WHERE AllianceID=? and EventID=? ORDER by EventPoints DESC LIMIT 40", (alliance['AllianceID'], EventID))
            else:
                eventscores = myDB.select("SELECT * from eventresults WHERE AllianceID = ? and EventID = ?", (alliance['AllianceID'], EventID))
            for score in eventscores:
                try:
                    tempScore += int(score['EventPoints'])
                    totalScore += int(score['EventPoints'])
                except ValueError:
                    pass

            controlValueDict = {'AllianceID': alliance['AllianceID'],
                                'EventID': EventID,}
            newValueDict = {'AlliancePoints': tempScore,
                            'TimeCompleted': allianceevent['TimeCompleted'],
                            }
            myDB.upsert('allianceevents', newValueDict, controlValueDict)
    controlValueDict = {'EventID': EventID,}
    newValueDict = {'EventTotalScore': totalScore,}
    myDB.upsert('events', newValueDict, controlValueDict)

def updateAlliancePlayers(AllianceID):
    if AllianceID > -1:
        myDB = database.DBConnection()
        players = myDB.select("SELECT * from players WHERE AllianceID = ?", (AllianceID,))
        medals = 0
        playercount = 0
        for player in players:
            try:
                medals += int(str(player['MedalCount']).strip() or 0)
            except ValueError:
                pass
            updatePlayer(player['PlayerID'])
            playercount += 1
        controlValueDict = {'AllianceID': AllianceID}
        newValueDict = {'MedalCount': medals,
                        'PlayerCount': playercount,}
        myDB.upsert("alliances",newValueDict, controlValueDict)

def updateAllianceWar(WarID):
    myDB=database.DBConnection()
    scores = myDB.select("SELECT * from warscores WHERE WarID = ?", (WarID,))
    totalscore = 0
    for score in scores:
        totalscore += score['WarScore']
    controlValueDict = {'WarID': WarID}
    newValueDict = {'TotalWarScore': totalscore}
    myDB.upsert("wars",newValueDict,controlValueDict)

def updateAllianceEventPlanning(EventID):
    myDB = database.DBConnection()
    event = myDB.match("SELECT * from events where EventID=?", (EventID,))
    alliances = myDB.select("SELECT * from alliances")
    for alliance in alliances:
        if alliance['AllianceID'] > -1:
            eventplanning = myDB.match("SELECT * from eventplanning WHERE AllianceID=? and EventID=?",
                                       (alliance['AllianceID'], EventID))
            if len(eventplanning) == 0:
                planningID = uuid.uuid4().hex
                controlValueDict = {'AllianceID': alliance['AllianceID'],
                                    'EventID': EventID, }
                newValueDict = {'PlanUUID': planningID,
                                'AllianceMinimumPoints': alliance['MinimumEventScore'], }
                logger.debug('Creating alliance event planning: %s [%s]' % (event['EventName'], alliance['AllianceName']))
                myDB.upsert('eventplanning', newValueDict, controlValueDict)
    globalplan = myDB.match("SELECT * from eventplanning WHERE AllianceID=-3 and EventID=?", (EventID,))
    if len(globalplan) == 0:
        planningID = uuid.uuid4().hex
        controlValueDict = {'AllianceID': -3,
                            'EventID': EventID,}
        newValueDict = {'PlanUUID': planningID,}
        myDB.upsert('eventplanning', newValueDict, controlValueDict)

def getCommitment(PlayerID=None, AllianceID=None, EventID=None, Total=False):
    if PlayerID and EventID:
        myDB = database.DBConnection()
        cmd = "SELECT * from eventcommitments WHERE PlayerID=? and EventID=?"
        args = [PlayerID, EventID]
        if AllianceID:
            cmd += " and AllianceID=?"
            args.append(AllianceID)
        rowlist = myDB.select(cmd, tuple(args))
        totalcommit = 0
        commits = []
        for row in rowlist:
            totalcommit += row['CommittedPoints']
            commits.append(row['CommittedPoints'])
        if Total:
            return totalcommit
        else:
            if len(commits) <= 1:
                return totalcommit
            else:
                return commits

def getEventResults(PlayerID, EventID, AllianceID=None, Total=False):
    myDB = database.DBConnection()
    cmd = "SELECT * from eventresults WHERE PlayerID=? and EventID=?"
    args = [PlayerID, EventID]
    if AllianceID:
        cmd += " and AllianceID=?"
        args.append(AllianceID)
    rowlist = myDB.select(cmd, tuple(args))
    totalpoints = 0
    points = []
    for row in rowlist:
        totalpoints += row['EventPoints']
        points.append(row['EventPoints'])
    if Total:
        return totalpoints
    else:
        if len(points) <= 1:
            return totalpoints
        else:
            return points


def getCommitmentResults(PlayerID, EventID, AllianceID=None):
    myDB = database.DBConnection()
    Total = True
    if AllianceID:
        Total = False
    commit = getCommitment(PlayerID, AllianceID, EventID, Total)
    results = getEventResults(PlayerID, EventID, AllianceID, Total)
    if results == 0:
        percent = 0
    elif commit == 0:
        percent = -1
    else:
        percent = results/commit
    returnresults = {
        'Commit': commit,
        'Points': results,
        'Percent': percent
    }
    return returnresults

def getEventRoster(EventID, AllianceID):
    myDB = database.DBConnection()
    cmd = "SELECT players.playerID, players.PlayerName, players.AllianceID, alliances.AllianceName, events.EventID, EventName, CommittedPoints, AvailFriday, AvailSaturday, AvailSunday, WillingToMove, Notes, NewAllianceID FROM players,alliances,events NATURAL LEFT JOIN eventcommitments NATURAL LEFT JOIN eventplanningmovements WHERE players.AllianceID = alliances.AllianceID and EventID=?"
    args = [EventID]
    rowlist = myDB.select(cmd, tuple(args))
    roster = []
    commitment = 0
    allianceroster = {}
    alliance = getAlliance(AllianceID)
    if len(rowlist):
        for row in rowlist:
            all = -3
            if row['NewAllianceID']:
                all = row['NewAllianceID']
            else:
                all = row['AllianceID']
            if int(all) == int(AllianceID):
                temprow = {}
                temprow['PlayerName'] = row['PlayerName']
                temprow['CommittedPoints'] = row['CommittedPoints']
                temprow['AvailFriday'] = intToYesNo(row['AvailFriday'])
                temprow['AvailSaturday'] = intToYesNo(row['AvailSaturday'])
                temprow['AvailSunday'] = intToYesNo(row['AvailSunday'])
                temprow['WillingToMove'] = intToYesNo(row['WillingToMove'])
                temprow['Notes'] = row['Notes']
                temprow['PlayerID'] = row['PlayerID']
                if row['CommittedPoints']:
                    commitment += int(row['CommittedPoints'])
                roster.append(temprow)
    allianceroster['AllianceName'] = alliance['AllianceName']
    allianceroster['TotalCommitment'] = commitment
    allianceroster['Roster'] = roster
    return allianceroster

def getPlayer(PlayerID):
    myDB = database.DBConnection()
    playerrow = myDB.match("SELECT * from players WHERE PlayerID=?", (PlayerID,))
    logger.debug(playerrow)
    player = {}
    if len(playerrow):
        for key in playerrow.keys():
            if not playerrow[key] or playerrow[key] == uni('None'):
                player[key] = ""
            else:
                player[key] = playerrow[key]
    else:
        player['PlayerName'] = 'DeletedPlayer'
        player['PlayerID'] = -3
    return player

def getAlliance(AllianceID):
    myDB = database.DBConnection()
    alliancerow = myDB.match("SELECT * from alliances WHERE AllianceID=?", (AllianceID,))
    alliance = {}
    if len(alliancerow):
        for key in alliancerow.keys():
            if not alliancerow[key] or alliancerow[key] == uni('None'):
                alliance[key] = ''
            else:
                alliance[key] = alliancerow[key]
    else:
        alliance['AllianceName'] = 'DeletedAlliance'
        alliance['AllianceID'] = -3
    return alliance

def getBaseIndex(IndexID):
    myDB = database.DBConnection()
    indexrow = myDB.match("SELECT * from gunmatrix WHERE IndexID=?", (IndexID,))
    playerinfo = myDB.match("SELECT PlayerID, PlayerName, players.AllianceID, alliances.AllianceName from players,alliances WHERE players.AllianceID = alliances.AllianceID and PlayerID=?", (indexrow['PlayerID'],))
    index = {}
    if len(indexrow) > 0:
        index['IndexID'] = indexrow['IndexID']
        index['HQLevel'] = indexrow['HQLevel']
        index['PlayerLevel'] = indexrow['PlayerLevel']
        index['MedalCount'] = indexrow['MedalCount']
        index['BuildBots'] = indexrow['BuildBots']
        index['IndexDate'] = indexrow['IndexDate']
        index['PlayerName'] = playerinfo['PlayerName']
        index['PlayerID'] = playerinfo['PlayerID']
        index['AllianceName'] = playerinfo['AllianceName']
        index['AllianceID'] = playerinfo['AllianceID']
        defensegroups = []
        tempdefenses = []
        for group in alliancekeeper.DEFENSES:
            defensegroup = {'Name': group['Label']}
            defensecount = 0
            defenses = []
            for row in group['Row']:
                if int(row['MinHQ']) <= int(indexrow['HQLevel']):
                    defensecount += 1
                    level = indexrow[row['Field']]
                    maxlevel = indexrow['HQLevel'] - row['LevelOffset']
                    if isinstance(level, (int, long)):
                        pass
                    else:
                        level = 0
                    percent = round((float(level)/float(maxlevel)*100),0)
                    maxedlevel = 16 - row['LevelOffset']
                    percentofmaxed = round((float(level)/float(maxedlevel)*100),0)
                    tempdefenses.append(percentofmaxed)
                    behind = maxlevel - level
                    defense = {
                        'Name': row['Field'],
                        'Level': level,
                        'MaxLevel': maxlevel,
                        'Percent': percent,
                        'Behind': behind,
                        'PercentOfMaxed': percentofmaxed,
                    }
                    defenses.append(defense)
            defensegroup['Count'] = defensecount
            defensegroup['Defenses'] = defenses
            defensegroups.append(defensegroup)
        index['DefenseGroups'] = defensegroups
        defenseindex = 0
        for percentage in tempdefenses:
            defenseindex += percentage
        defenseindex = round(defenseindex / len(tempdefenses), 0)
        index['DefenseIndex'] = defenseindex
        levelcount = 0
        totalwalls = 0
        wallgroup = {}
        walls = []
        for i in xrange(int(indexrow['HQLevel'])):
            lev = i + 1
            field = 'Wall%s' % lev
            val = indexrow[field]
            if isinstance(val, (int, long)):
                pass
            else:
                val = 0
            if int(val) > 0:
                levelcount += 1
                totalwalls += int(indexrow[field])
                maxlevel = indexrow['HQLevel']
                behind = maxlevel - lev
                wall = {
                    'Name': field,
                    'Count': indexrow[field],
                    'Level': lev,
                    'MaxLevel': maxlevel,
                    'Behind': behind,
                }
                walls.append(wall)
        wallgroup['WallLevels'] = levelcount
        wallgroup['TotalWalls'] = totalwalls
        wallgroup['Walls'] = walls
        index['WallGroup'] = wallgroup
    return index

def updatePlayer(PlayerID):
    myDB = database.DBConnection()
    newestindex = myDB.match('SELECT * from gunmatrix WHERE PlayerID=? ORDER BY IndexDate DESC', (PlayerID,))
    controlValueDict = {'PlayerID': PlayerID, }
    newValueDict = {}
    if len(newestindex) > 0:
        tempindex = getBaseIndex(newestindex['IndexID'])
        newValueDict['HeadQuarters'] = newestindex['HQLevel']
        newValueDict['PlayerLevel'] = newestindex['PlayerLevel']
        newValueDict['MedalCount'] = newestindex['MedalCount']
        newValueDict['PlayerGunIndex'] = tempindex['DefenseIndex']
    wars = myDB.select('SELECT * from warscores WHERE PlayerID=?', (PlayerID,))
    warCount = 0
    for war in wars:
        if int(war['WarScore']) != 0 or int(war['WarAttemptsLeft']) != 3:
            warCount +=1
    events = myDB.select('SELECT * from eventresults WHERE PlayerID=?', (PlayerID,))
    eventlist = []
    for event in events:
        if event['EventID'] not in eventlist:
            eventlist.append(event['EventID'])
        logger.debug(eventlist)
    eventCount = len(eventlist)
    newValueDict['EventCount'] = eventCount
    newValueDict['WarCount'] = warCount
    newValueDict['WarAverage'] = getWarAverage(PlayerID)
    myDB.upsert("players", newValueDict, controlValueDict)

def getWarAverage(PlayerID, NumberOfDays = -1, WarLeague=None, WarList=None):
    myDB = database.DBConnection()
    if NumberOfDays == -1:
        NumberOfDays = alliancekeeper.CONFIG['WAR_AVERAGE_HISTORY']
    if WarList:
        NumberOfDays = 0
    oldestDay = todayMinusDays(NumberOfDays)
    cmd = 'SELECT PlayerID, WarScore, warscores.WarID from warscores, wars where warscores.WarID = wars.WarID and warscores.PlayerID=?'
    args = [PlayerID]
    if WarLeague:
        cmd += ' and wars.WarLeague=?'
        args.append(WarLeague)
    if NumberOfDays > 0:
        cmd += ' and wars.WarStartTime >= ?'
        args.append(oldestDay)
    rowlist = myDB.select(cmd, tuple(args))
    if WarList:
        templist = list(rowlist)
        rowlist = []
        for row in templist:
            if row['WarID'] in WarList:
                rowlist.append(row)
    if len(rowlist) > 0:
        total = 0
        count = len(rowlist)
        for score in rowlist:
            total += score['WarScore']
        return int(total/count)
    else:
        return 0


def getPlayerRecentWars(PlayerID, WarCount=3):
    myDB = database.DBConnection()
    recentWars = myDB.select('SELECT PlayerID, WarScore, WarAttemptsLeft, AllianceID, warscores.WarID, WarOpponent, WarStartTime from wars, warscores where warscores.WarID = wars.WarID and warscores.PlayerID = ? ORDER BY WarStartTime DESC LIMIT ?', (PlayerID, WarCount))
    wars = []
    if len(recentWars) > 0:
        for recentWar in recentWars:
            war = {
            'PlayerID': recentWar['PlayerID'],
            'WarScore': recentWar['WarScore'],
            'WarAttemptsLeft': recentWar['WarAttemptsLeft'],
            'AllianceID': recentWar['AllianceID'],
            'WarID': recentWar['WarID'],
            'WarOpponent': recentWar['WarOpponent'],
            'WarStartTime': recentWar['WarStartTime'],
            }
            wars.append(war)
    return wars

def getEventList(EventType="Leaderboard", Count=10, ByName=False):
    myDB = database.DBConnection()
    events = myDB.select('SELECT EventID, EventName FROM events WHERE EventType=? ORDER BY EventStartTime DESC LIMIT ?', (EventType, str(Count),))
    results = []
    for result in events:
        if ByName:
            results.append(result['EventName'])
        else:
            results.append(result['EventID'])
    return results

def getPlayers(AllianceID=None):
    myDB = database.DBConnection()
    if AllianceID:
        sqlstring = "SELECT PlayerID, PlayerName, AllianceID FROM players WHERE AllianceID=%s" % AllianceID
    else:
        sqlstring = "SELECT PlayerID, PlayerName, AllianceID FROM players WHERE AllianceID>0"
    players = myDB.select(sqlstring)
    results = []
    for player in players:
        tempplayer = {}
        for key in player.keys():
            tempplayer[key] = player[key]
        results.append(tempplayer)
    return results

def getEventParticipation(EventType="Leaderboard", AllianceID=None, Count=10):
    myDB = database.DBConnection()
    events = getEventList(EventType, Count)
    players = getPlayers(AllianceID)
    returnlist = []
    for player in players:
        if AllianceID:
            playerinfo = ['<a href="playerPage?PlayerID=%s">%s</a>' % (player['PlayerID'], player['PlayerName'])]
        else:
            alliance = getAlliance(AllianceID)
            playerinfo = ['<a href="playerPage?PlayerID=%s">%s (%s)</a>' % (player['PlayerID'], player['PlayerName'], alliance['AllianceID'])]
        for event in events:
            playerinfo.append(getCommitmentResults(player['PlayerID'], event, AllianceID=AllianceID))
        returnlist.append(playerinfo)
    return returnlist

def getWarDetails(warid):
    myDB = database.DBConnection()
    warscores = myDB.select('SELECT * from warscores WHERE warid=?', (warid,))
    topscore = 0
    for score in warscores:
        if score['WarScore'] > topscore:
            topscore = score['WarScore']
    topscorers_db = myDB.select('SELECT PlayerName, WarScore, WarAttemptsLeft from warscores NATURAL JOIN players where warid=? AND WarScore=?', (warid, topscore,))
    noshows_db = myDB.select('SELECT PlayerName, WarScore, WarAttemptsLeft from warscores NATURAL JOIN players where warid=? AND WarScore=0 AND WarAttemptsLeft=3', (warid,))
    topscorers = []
    noshows = []
    for score in noshows_db:
        scorer = {
            'PlayerName': score['PlayerName'],
            'WarScore': score['WarScore'],
            'WarAttemptsLeft': score['WarAttemptsLeft'],
        }
        noshows.append(scorer)
    for score in topscorers_db:
        scorer = {
            'PlayerName': score['PlayerName'],
            'WarScore': score['WarScore'],
            'WarAttemptsLeft': score['WarAttemptsLeft'],
        }
        topscorers.append(scorer)
    participants = len(warscores) - len(noshows)
    wardetails = {
        'topscorers': topscorers,
        'noshows': noshows,
        'participants': participants,
        'topscore': topscore,
    }
    return wardetails

def getUserPrefs(userid=None, key=None):
    myDB = database.DBConnection()
    if not userid:
        return alliancekeeper.DEFAULT_PREFS
    else:
        user = myDB.match('SELECT Prefs from users where UserID=?', (userid,))
        if len(user) > 0 and user['Prefs']:
            prefs = simplejson.loads(user['Prefs'])
        else:
            prefs = {}
        for prefkey in alliancekeeper.DEFAULT_PREFS.keys():
            if prefkey not in prefs.keys():
                prefs[prefkey] = alliancekeeper.DEFAULT_PREFS[prefkey]
        if key:
            if key in prefs.keys():
                return prefs[key]
            else:
                return None
        else:
            return prefs

def exportWarDetails(AllianceID, starttime, endtime):
    myDB = database.DBConnection()
    tempwarstats = myDB.select('SELECT alliances.AllianceName, players.PlayerName, wars.WarStartTime, wars.WarOpponent, wars.WarLeague, warscores.WarScore, warscores.WarAttemptsLeft FROM wars, warscores, players, alliances WHERE warscores.WarID = wars.WarID AND warscores.PlayerID = players.PlayerID AND alliances.AllianceID = wars.AllianceID AND wars.AllianceID = ? AND wars.WarStartTime >= ? AND wars.WarStartTime <= ?', (AllianceID, starttime, endtime,))
    warstats = []
    allianceName = ''
    for stat in tempwarstats:
        allianceName = stat['AllianceName']
        newstat = {
            'AllianceName': stat['AllianceName'],
            'PlayerName': stat['PlayerName'],
            'WarStartTime': stat['WarStartTime'],
            'WarOpponent': stat['WarOpponent'],
            stat['WarLeague']: stat['WarScore'],
            'WarAttemptsLeft': stat['WarAttemptsLeft']
        }
        warstats.append(newstat)
    logger.debug(warstats)
    cacheLocation = alliancekeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'Export Alliance Wars - %s.csv' % allianceName
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    fieldnames = ['AllianceName', 'PlayerName', 'WarStartTime', 'WarOpponent', 'Junkion', 'Carbon', 'Bronze', 'Silver', 'Gold', 'Platinum', 'Caminus', 'Cybertron', 'Prime', 'WarAttemptsLeft']
    try:
        with open(fileloc, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, encoding='utf-8')
            writer.writeheader()
            sortedcsv = sorted(warstats, key=lambda k: (k['PlayerName'], k['WarStartTime']))
            for line in sortedcsv:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg
