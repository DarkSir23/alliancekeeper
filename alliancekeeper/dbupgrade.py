
from __future__ import with_statement

import datetime
import os
import shutil
import threading
import time
import traceback

import alliancekeeper
from alliancekeeper import logger, database
from alliancekeeper.common import restartJobs, currentTime


def upgrade_needed():
    """
    Check if database needs upgrading
    Return zero if up-to-date
    Return current version if needs upgrade
    """

    myDB = database.DBConnection()
    db_version = 0
    result = myDB.match('PRAGMA user_version')
    if result and result[0]:
        value = str(result[0])
        if value.isdigit():
            db_version = int(value)

    # database version history:
    # 0 original version or new empty database

    db_current_version = 11
    if db_version < db_current_version:
        return db_current_version
    return 0


def has_column(myDB, table, column):
    columns = myDB.select('PRAGMA table_info(%s)' % table)
    if not columns:  # no such table
        return False
    for item in columns:
        if item[1] == column:
            return True
    # no such column
    return False


def dbupgrade(db_current_version):
    with open(os.path.join(alliancekeeper.CONFIG['LOGDIR'], 'dbupgrade.log'), 'a') as upgradelog:
        try:
            myDB = database.DBConnection()
            db_version = 0
            result = myDB.match('PRAGMA user_version')
            if result and result[0]:
                value = str(result[0])
                if value.isdigit():
                    db_version = int(value)

            check = myDB.match('PRAGMA integrity_check')
            if check and check[0]:
                result = check[0]
                if result == 'ok':
                    logger.debug('Database integrity check: %s' % result)
                else:
                    logger.error('Database integrity check: %s' % result)
                    # should probably abort now

            if db_version < db_current_version:
                myDB = database.DBConnection()
                if db_version:
                    alliancekeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(alliancekeeper.UPDATE_MSG)
                    upgradelog.write("%s v0: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                else:
                    if not has_column(myDB, "alliances", "AllianceID"):
                        # it's a new database. Create tables but no need for any upgrading
                        db_version = db_current_version
                        alliancekeeper.UPDATE_MSG = 'Creating new database, version %s' % db_version
                        upgradelog.write("%s v0: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        logger.info(alliancekeeper.UPDATE_MSG)

                    myDB.action('CREATE TABLE IF NOT EXISTS alliances (AllianceID INTEGER PRIMARY KEY AUTOINCREMENT, \
                        AllianceName TEXT UNIQUE, DateAdded TEXT, PlayerCount int, MedalCount int, \
                        MinimumEventScore INTEGER, CommanderID INTEGER, Officers TEXT, WarLeague TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS players (AllianceID INTEGER, PlayerName TEXT, LineID TEXT, \
                        PlayerID INTEGER PRIMARY KEY AUTOINCREMENT, PlayerDateAdded TEXT, LineDisplay TEXT, \
                        PlayerGunIndex DOUBLE, EventCount INTEGER, WarCount INTEGER, WarAverage INTEGER, PlayerLevel INTEGER, \
                        MedalCount INTEGER, HeadQuarters INTEGER, MaxSquad INTEGER, Note TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS events (EventID INTEGER PRIMARY KEY AUTOINCREMENT, \
                        EventName TEXT, EventType TEXT, TimeStamp TEXT, EventStartTime TEXT, EventTotalScore INTEGER)')
                    myDB.action('CREATE TABLE IF NOT EXISTS allianceevents (EventID INTEGER, AllianceID INTEGER, \
                        AlliancePoints INTEGER, TimeCompleted TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS eventresults (EventID int, PlayerID int, AllianceID int, \
                        EventPoints int)')
                    myDB.action('CREATE TABLE IF NOT EXISTS wars (WarID INTEGER PRIMARY KEY AUTOINCREMENT, \
                        AllianceID int, WarOpponent TEXT, WarLeague TEXT, TimeStamp TEXT, WarStartTime TEXT, TotalWarScore INTEGER, \
                        OpponentWarScore INTEGER)')
                    myDB.action('CREATE TABLE IF NOT EXISTS warscores (WarID int, PlayerID int, WarScore int, \
                        WarAttemptsLeft int, TimeStamp TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS playeractivity (TimeStamp TEXT, PlayerID int,  \
                        ActivityDate TEXT, ActivityDesc TEXT, UserName TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS allianceactivity (TimeStamp TEXT, AllianceID int, \
                        ActivityDate TEXT, ActivityDesc TEXT, UserName TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS gunmatrix (PlayerID int, \
                        HQLevel int, MedalCount int, PlayerLevel int, BuildBots int, \
                        Wall1 int, Wall2 int, Wall3 int, Wall4 int, Wall5 int, Wall6 int, Wall7 int, Wall8 int, \
                        Wall9 int, Wall10 int, Wall11 int, Wall12 int, Wall13 int, Wall14 int, Wall15 int, Wall16 int, Wall17 int,\
                        AutoCannon1 int, AutoCannon2 int, AutoCannon3 int, AutoCannon4 int, AutoCannon5 int, AutoCannon6 int, \
                        LaserTurret1 int, LaserTurret2 int, LaserTurret3 int, LaserTurret4 int, LaserTurret5 int, \
                        Mortar1 int, Mortar2 int, Mortar3 int, Mortar4 int, \
                        ShockTower1 int, ShockTower2 int, ShockTower3 int, \
                        BeamLaser1 int, BeamLaser2 int, BeamLaser3 int, \
                        Outpost1 int, Outpost2 int, Outpost3 int, Outpost4 int, \
                        MissileLauncher1 int, MissileLauncher2 int, MissileLauncher3 int, Scanner int, Shuttle int, \
                        TimeStamp TEXT, IndexDate TEXT, IndexID INTEGER PRIMARY KEY AUTOINCREMENT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS users (UserID INTEGER PRIMARY KEY AUTOINCREMENT, \
                        UserName TEXT UNIQUE, PWHash TEXT, InterfaceType TEXT, InterfaceLook TEXT, AccessLevel INTEGER, Prefs TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS useractivity (TimeStamp TEXT, UserID int, ActivityDate TEXT, \
                                ActivityDesc TEXT, UserName TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS eventplanning (EventID INTEGER, AllianceID INTEGER, PlanUUID TEXT, \
                        AllianceMinimumPoints INTEGER)')
                    myDB.action('CREATE TABLE IF NOT EXISTS eventcommitments (EventID INTEGER, AllianceID INTEGER, \
                        PlayerID INTEGER, WillingToMove INTEGER, CommittedPoints INTEGER, AvailFriday INTEGER, \
                        AvailSaturday INTEGER, AvailSunday INTEGER, Notes TEXT)')
                    myDB.action('CREATE TABLE IF NOT EXISTS eventplanningmovements (EventID INTEGER, PlayerID INTEGER, \
                        NewAllianceID INTEGER)')

                    myDB.upsert("alliances", {'AllianceName': "Outside Alliance",
                                              'PlayerCount': 0,
                                              'MedalCount': 0,
                                              'DateAdded': currentTime()},
                                {'AllianceID': -1})
                    myDB.upsert("alliances", {'AllianceName': "BlackList",
                                              'PlayerCount': 0,
                                              'MedalCount': 0,
                                              'DateAdded': currentTime()},
                                {'AllianceID': -2})

                # These are the incremental changes before database versioning was introduced.
                # Old database tables might already have these incorporated depending on version, so we need to check...
                if db_version < 10:
                    alliancekeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(alliancekeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))

                    if not has_column(myDB, "alliances", "PlayerCount"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track player count.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V2: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE alliances ADD COLUMN PlayerCount int')

                    if not has_column(myDB, "alliances", "MedalCount"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track medal count.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V2: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE alliances ADD COLUMN MedalCount int')

                    if not has_column(myDB, "players", "MaxSquad"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track medal count.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V3: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE players ADD COLUMN MaxSquad INTEGER')

                    if not has_column(myDB, "wars", "TotalWarScore"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track alliance war score.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V4: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE wars ADD COLUMN TotalWarScore INTEGER')

                    if not has_column(myDB, "wars", "OpponentWarScore"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track alliance opponent score.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V4: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE wars ADD COLUMN OpponentWarScore INTEGER')

                    alliancekeeper.UPDATE_MSG='Updating database to contain Blacklist and Outside Alliance.'
                    logger.debug(alliancekeeper.UPDATE_MSG)
                    upgradelog.write("%s V5: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                    myDB.upsert("alliances",{'AllianceName': "Outside Alliance",
                                             'PlayerCount': 0,
                                             'MedalCount': 0,
                                             'DateAdded': currentTime()},
                                {'AllianceID': -1})
                    myDB.upsert("alliances",{'AllianceName': "BlackList",
                                             'PlayerCount': 0,
                                             'MedalCount': 0,
                                             'DateAdded': currentTime()},
                                {'AllianceID': -2})

                    if not has_column(myDB, "alliances", "MinimumEventScore"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to store default minimum event scores in alliances.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V6: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE alliances ADD COLUMN MinimumEventScore INTEGER')
                    if db_version < 6:
                        alliancekeeper.UPDATE_MSG="Updating database to handle Event Planning."
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V6: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('CREATE TABLE IF NOT EXISTS eventplanning (EventID INTEGER, AllianceID INTEGER, PlanUUID TEXT, \
                            AllianceMinimumPoints INTEGER)')
                        myDB.action('CREATE TABLE IF NOT EXISTS eventcommitments (EventID INTEGER, AllianceID INTEGER, \
                            PlayerID INTEGER, WillingToMove INTEGER, CommittedPoints INTEGER, AvailFriday INTEGER, \
                            AvailSaturday INTEGER, AvailSunday INTEGER, Notes TEXT)')
                        myDB.action('CREATE TABLE IF NOT EXISTS eventplanningmovements (EventID INTEGER, PlayerID INTEGER, \
                            NewAllianceID INTEGER)')
                    if not has_column(myDB, "gunmatrix", "IndexID"):
                        alliancekeeper.UPDATE_MSG="Updating database to add Base Index IndexID."
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V7: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('DROP TABLE gunmatrix')
                        myDB.action('CREATE TABLE IF NOT EXISTS gunmatrix (PlayerID int, \
                            HQLevel int, MedalCount int, PlayerLevel int, BuildBots int, \
                            Wall1 int, Wall2 int, Wall3 int, Wall4 int, Wall5 int, Wall6 int, Wall7 int, Wall8 int, \
                            Wall9 int, Wall10 int, Wall11 int, Wall12 int, Wall13 int, Wall14 int, Wall15 int, Wall16 int, \
                            AutoCannon1 int, AutoCannon2 int, AutoCannon3 int, AutoCannon4 int, AutoCannon5 int, AutoCannon6 int, \
                            LaserTurret1 int, LaserTurret2 int, LaserTurret3 int, LaserTurret4 int, LaserTurret5 int, \
                            Mortar1 int, Mortar2 int, Mortar3 int, Mortar4 int, \
                            ShockTower1 int, ShockTower2 int, ShockTower3 int, \
                            BeamLaser1 int, BeamLaser2 int, BeamLaser3 int, \
                            Outpost1 int, Outpost2 int, Outpost3 int, Outpost4 int, \
                            MissileLauncher1 int, MissileLauncher2 int, MissileLauncher3 int, Scanner int, Shuttle int, \
                            TimeStamp TEXT, IndexDate TEXT, IndexID INTEGER PRIMARY KEY AUTOINCREMENT)')
                    if not has_column(myDB, "players", "Note"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to handle Player Notes'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V8: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE players ADD COLUMN Note TEXT')
                    if not has_column(myDB, "alliances", "CommanderID"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to handle Alliance Roles'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V8: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE alliances ADD COLUMN CommanderID INTEGER')
                        myDB.action('ALTER TABLE alliances ADD COLUMN Officers TEXT')
                    if not has_column(myDB, "players", "WarAverage"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to handle War Averages'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V9: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE players ADD COLUMN WarAverage INTEGER')
                    if not has_column(myDB, "wars", "WarLeague"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track War Leagues'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V9: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE wars ADD COLUMN WarLeague TEXT')
                    if not has_column(myDB, "alliances", "WarLeague"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track War Leagues'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V9: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE alliances ADD COLUMN WarLeague TEXT')
                    if not has_column(myDB, "users", "Prefs"):
                        alliancekeeper.UPDATE_MSG = 'Updating database to track User Preferences'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V10: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE users ADD COLUMN Prefs TEXT')
                if db_version < 11:
                    alliancekeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(alliancekeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))

                    if not has_column(myDB, "gunmatrix", "Wall17"):
                        alliancekeeper.UPDATE_MSG = 'Adding HQ 17 Walls.'
                        logger.debug(alliancekeeper.UPDATE_MSG)
                        upgradelog.write("%s V2: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE gunmatrix ADD COLUMN Wall17 int')

                myDB.action('PRAGMA user_version=%s' % db_current_version)
                alliancekeeper.UPDATE_MSG = 'Cleaning Database'
                upgradelog.write("%s: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))
                myDB.action('vacuum')
                alliancekeeper.UPDATE_MSG = 'Database upgraded to version %s' % db_current_version
                logger.info(alliancekeeper.UPDATE_MSG)
                upgradelog.write("%s: %s\n" % (time.ctime(), alliancekeeper.UPDATE_MSG))

                restartJobs(start='Start')

            alliancekeeper.UPDATE_MSG = ''

        except Exception:
            msg = 'Unhandled exception in database upgrade: %s' % traceback.format_exc()
            upgradelog.write("%s: %s\n" % (time.ctime(), msg))
            logger.error(msg)
            alliancekeeper.UPDATE_MSG = ''
