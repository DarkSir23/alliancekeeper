import logging
import os
import threading
import time
from datetime import datetime

from logging import handlers


import alliancekeeper
from alliancekeeper import formatter, database


class DBLogger(object):
    def __init__(self):
        placeholder = None

    @staticmethod
    def dblog(message, level, userName='', id=-1):
        myDB = database.DBConnection()
        message = formatter.safe_unicode(message).encode(alliancekeeper.SYS_ENCODING)
        enactor = None
        logdb = None
        if level == 'USER':
            enactor = 'UserID'
            logdb = 'useractivity'
        elif level == 'PLAYER':
            enactor = 'PlayerID'
            logdb = 'playeractivity'
        elif level == 'ALLIANCE':
            enactor = 'AllianceID'
            logdb = 'allianceactivity'
        if enactor and logdb:
            timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M')
            activitydate = datetime.now().strftime('%Y-%m-%dT%H:%M')
            myDB.action('insert into %s values (?, ?, ?, ?, ?)' % logdb, (timestamp, id, activitydate, message, userName))


alliancekeeper_dblog = DBLogger()


def user(message, userName='', id=-1):
    alliancekeeper_dblog.dblog(message, level='USER', userName=userName, id=id)


def player(message, userName='', id=-1):
    alliancekeeper_dblog.dblog(message, level='PLAYER', userName=userName, id=id)


def alliance(message, userName='', id=-1):
    alliancekeeper_dblog.dblog(message, level='ALLIANCE', userName=userName, id=id)
