from __future__ import with_statement

import ConfigParser
import threading
import cherrypy
import subprocess
import sys
import os
import platform
import time
import json


from alliancekeeper import logger, database, wars
from lib.apscheduler.scheduler import Scheduler
from alliancekeeper.formatter import check_int
from alliancekeeper.cache import fetchURL
from alliancekeeper.common import checkRunningJobs

FULL_PATH = None
PROG_DIR = None
DAEMON = False
SIGNAL = None
PIDFILE = ''
DATADIR = ''
CACHEDIR = ''
SESSIONDIR = ''
CONFIGFILE = ''
LOGLEVEL = 1
CONFIG = {}
CFG = ''
DBFILE = None
COMMIT_LIST =None
SYS_ENCODING = 'utf-8'
UPDATE_MSG = ''
HTTP_ROOT = ''
CURRENT_TAB = '1'
BOOTSTRAP_THEMELIST = []
EVENT_TYPES = ['Leaderboard', 'Leaderboard Classic', 'Totalizer', 'Totalizer with Prestiges']
WAR_LEAGUES = ['Junkion', 'Carbon', 'Bronze', 'Silver', 'Gold', 'Platinum', 'Caminus', 'Cybertron', 'Prime', 'Hard', 'Awakens Hard/Tracked', 'Within Hard/Tracked', 'DJD Hard/Tracked']
DEFENSES = [
    {'Label': 'Auto Cannons',
     'MinDisplay': 1,
     'Class': 'AutoCannons',
     'Row': [
        {'Field': 'AutoCannon1', 'MinHQ': 1, 'LevelOffset': 0, 'ColumnSize': '2'},
        {'Field': 'AutoCannon2', 'MinHQ': 3, 'LevelOffset': 0, 'ColumnSize': '2'},
        {'Field': 'AutoCannon3', 'MinHQ': 5, 'LevelOffset': 0, 'ColumnSize': '2'},
        {'Field': 'AutoCannon4', 'MinHQ': 10, 'LevelOffset': 0, 'ColumnSize': '2'},
        {'Field': 'AutoCannon5', 'MinHQ': 14, 'LevelOffset': 0, 'ColumnSize': '2'},
        {'Field': 'AutoCannon6', 'MinHQ': 18, 'LevelOffset': 0, 'ColumnSize': '2'},
    ]},
    {'Label': 'Laser Turrets',
     'MinDisplay': 3,
     'Class': 'LaserTurrets',
     'Row': [
        {'Field': 'LaserTurret1', 'MinHQ': 3, 'LevelOffset': 2, 'ColumnSize': '3'},
        {'Field': 'LaserTurret2', 'MinHQ': 5, 'LevelOffset': 2, 'ColumnSize': '3'},
        {'Field': 'LaserTurret3', 'MinHQ': 9, 'LevelOffset': 2, 'ColumnSize': '3'},
        {'Field': 'LaserTurret4', 'MinHQ': 14, 'LevelOffset': 2, 'ColumnSize': '3'},
        {'Field': 'LaserTurret5', 'MinHQ': 18, 'LevelOffset': 2, 'ColumnSize': '2'},
    ]},
    {'Label': 'Mortars',
     'MinDisplay': 4,
     'Class': 'Mortars',
     'Row': [
        {'Field': 'Mortar1', 'MinHQ': 4, 'LevelOffset': 3, 'ColumnSize': '3'},
        {'Field': 'Mortar2', 'MinHQ': 7, 'LevelOffset': 3, 'ColumnSize': '3'},
        {'Field': 'Mortar3', 'MinHQ': 10, 'LevelOffset': 3, 'ColumnSize': '3'},
        {'Field': 'Mortar4', 'MinHQ': 16, 'LevelOffset': 3, 'ColumnSize': '3'},
    ]},
    {'Label': 'Beam Lasers',
     'MinDisplay': 6,
     'Class': 'BeamLasers',
     'Row': [
        {'Field': 'BeamLaser1', 'MinHQ': 6, 'LevelOffset': 5, 'ColumnSize': '4'},
        {'Field': 'BeamLaser2', 'MinHQ': 12, 'LevelOffset': 5, 'ColumnSize': '4'},
        {'Field': 'BeamLaser3', 'MinHQ': 18, 'LevelOffset': 5, 'ColumnSize': '4'},
    ]},
    {'Label': 'Shock Towers',
     'MinDisplay': 9,
     'Class': 'ShockTowers',
     'Row': [
        {'Field': 'ShockTower1', 'MinHQ': 9, 'LevelOffset': 8, 'ColumnSize': '4'},
        {'Field': 'ShockTower2', 'MinHQ': 14, 'LevelOffset': 8, 'ColumnSize': '4'},
        {'Field': 'ShockTower3', 'MinHQ': 18, 'LevelOffset': 8, 'ColumnSize': '4'},
    ]},
    {'Label': 'Missile Launchers',
     'MinDisplay': 11,
     'Class': 'MissileLaunchers',
     'Row': [
        {'Field': 'MissileLauncher1', 'MinHQ': 11, 'LevelOffset': 10, 'ColumnSize': '4'},
        {'Field': 'MissileLauncher2', 'MinHQ': 15, 'LevelOffset': 10, 'ColumnSize': '4'},
        {'Field': 'MissileLauncher3', 'MinHQ': 18, 'LevelOffset': 10, 'ColumnSize': '4'},
    ]},
    {'Label': 'Outposts',
     'MinDisplay': 2,
     'Class': 'Outposts',
     'Row': [
        {'Field': 'Outpost1', 'MinHQ': 2, 'LevelOffset': 1, 'ColumnSize': '3'},
        {'Field': 'Outpost2', 'MinHQ': 6, 'LevelOffset': 1, 'ColumnSize': '3'},
        {'Field': 'Outpost3', 'MinHQ': 11, 'LevelOffset': 1, 'ColumnSize': '3'},
        {'Field': 'Outpost4', 'MinHQ': 16, 'LevelOffset': 1, 'ColumnSize': '3'},
    ]}
]
WALLS = [0,6,8,10,12,14,16,18,20,22,24,26,28,30,32,38,40,42,46]

SCHED = Scheduler()
INIT_LOCK = threading.Lock()
__INITIALIZED__ = False
started = False

LOGLIST = []
LOGTOGGLE = 2 # normal logging

CONFIG_GIT = ['GIT_REPO', 'GIT_USER', 'GIT_BRANCH', 'LATEST_VERSION', 'GIT_UPDATED', 'CURRENT_VERSION',
    'COMMITS_BEHIND', 'INSTALL_TYPE']
CONFIG_NONWEB = ['LOGFILES', 'LOGSIZE', 'LOGLIMIT']
CONFIG_NONDEFAULT = ['BOOTSTRAP_THEME']

CONFIG_DEFINITIONS = {
    # Name	Type	Section	Default
    'USER_ACCOUNTS': ('bool', 'General', 0),
    'LOGDIR': ('str', 'General', ''),
    'LOGLIMIT': ('int', 'General', 500),
    'LOGFILES': ('int', 'General', 10),
    'LOGSIZE': ('int', 'General', 204800),
    'LOGLEVEL': ('int', 'General', 1),
    'DISPLAYLENGTH': ('int', 'General', 10),
    'HTTP_PORT': ('int', 'General', 8339),
    'HTTP_HOST': ('str', 'General', '0.0.0.0'),
    'HTTP_USER': ('str', 'General', ''),
    'HTTP_PASS': ('str', 'General', ''),
    'HTTP_HASHED_PASSWORD': ('Bool', 'General', 0),
    'HTTP_PROXY': ('bool', 'General', 0),
    'HTTP_ROOT': ('str', 'General', '/'),
    'HTTP_LOOK': ('str', 'General', 'bootstrap'),
    'HTTPS_ENABLED': ('bool', 'General', 0),
    'HTTPS_CERT': ('str', 'General', ''),
    'HTTPS_KEY': ('str', 'General', ''),
    'PROXY_HOST': ('str', 'General', ''),
    'PROXY_TYPE': ('str', 'General', ''),
    'GIT_PROGRAM': ('str', 'General', ''),
    'GIT_USER': ('str', 'Git', 'DarkSir23'),
    'GIT_REPO': ('str', 'Git', 'alliancekeeper'),
    'GIT_BRANCH': ('str', 'Git', 'master'),
    'GIT_UPDATED': ('str', 'Git', ''),
    'INSTALL_TYPE': ('str', 'Git', ''),
    'CURRENT_VERSION': ('str', 'Git', ''),
    'LATEST_VERSION': ('str', 'Git', ''),
    'COMMITS_BEHIND': ('int', 'Git', 0),
    'VERSIONCHECK_INTERVAL': ('int', 'Git', '24'),
    'WARNOTIFY_INTERVAL': ('int', 'War', 5),
    'BOOTSTRAP_THEME': ('str', 'General', 'cerulean'),
    'API_ENABLED': ('bool', 'General', 0),
    'API_KEY': ('str', 'General', ''),
    'TOGGLES': ('bool', 'General', 1),
    'FAMILY_NAME': ('str', 'General', ''),
    'COMMIT_SHOWMOVE': ('bool', 'EventPlanning', True),
    'WAR_SUMMARY_COLUMNS': ('int', 'AllianceDefaults', 3),
    'WAR_AVERAGE_HISTORY': ('int', 'AllianceDefaults', 30),
}

DEFAULT_PREFS = {
    'SHOW_BASE_INDEX': 1,
    'SHOW_HEADQUARTERS': 1,
    'SHOW_MSP': 0,
    'SHOW_LEVEL': 1
}

def check_section(sec):
    if CFG.has_section(sec):
        return True
    else:
        CFG.add_section(sec)
        return False

def check_setting(cfg_type, cfg_name, item_name, def_val, log=True):
    """ Check option exists, corece to correct type, or return default """
    my_val = def_val
    if cfg_type == 'int':
        try:
            my_val = CFG.getint(cfg_name, item_name)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = int(def_val)
        except Exception as e:
            logger.warn('Invalid int for %s: %s, using default %s' % (cfg_name, item_name, int(def_val)))
            logger.debug(str(e))
            my_val = int(def_val)

    elif cfg_type == 'bool':
        try:
            my_val = CFG.getboolean(cfg_name, item_name)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = bool(def_val)
        except Exception as e:
            logger.warn('Invalid bool for %s: %s, using default %s' % (cfg_name, item_name, bool(def_val)))
            logger.debug(str(e))
            my_val = bool(def_val)

    elif cfg_type == 'str':
        try:
            my_val = CFG.get(cfg_name, item_name)
            if my_val.startswith('"') and my_val.endswith('"'):
                my_val = my_val[1:-1]
            if not len(my_val):
                my_val = def_val
            my_val = my_val.decode(SYS_ENCODING)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = str(def_val)
        except Exception as e:
            logger.warn('Invalid str for %s: %s, using default %s' % (cfg_name, item_name, str(def_val)))
            logger.debug(str(e))
            my_val = str(def_val)

    check_section(cfg_name)
    CFG.set(cfg_name, item_name, my_val)
    if log:
        logger.debug("%s : %s -> %s" % (cfg_name, item_name, my_val))

    return my_val


def initialize():
    global FULL_PATH, PROG_DIR, ARGS, DAEMON, SIGNAL, PIDFILE, DATADIR, CONFIGFILE, COMMIT_LIST, SYS_ENCODING, LOGLEVEL, \
        CONFIG, CFG, CACHEDIR, CURRENT_TAB, BOOTSTRAP_THEMELIST, DBFILE, __INITIALIZED__

    with INIT_LOCK:

        if __INITIALIZED__:
            return False

        check_section('General')
        # False to silence logging until logger initialzied
        for key in ['LOGLIMIT', 'LOGFILES', 'LOGSIZE']:
            item_type, section, default = CONFIG_DEFINITIONS[key]
            CONFIG[key.upper()] = check_setting(item_type, section, key.lower(), default, log=False)
        CONFIG['LOGDIR'] = os.path.join(DATADIR, 'logs')

        # Create logdir
        if not os.path.exists(CONFIG['LOGDIR']):
            try:
                os.makedirs(CONFIG['LOGDIR'])
            except OSError as e:
                if LOGLEVEL:
                    print '%s : unable to create folder for logs: %s' % (
                        CONFIG['LOGDIR'], str(e))

        # Start the logger, silence console logging if we need to
        CFGLOGLEVEL = check_setting('int', 'General', 'loglevel', 9, log=False)
        if LOGLEVEL == 1: # default if no debug or quiet on cmdline
            if CFGLOGLEVEL == 9:  # default value if none in config
                LOGLEVEL = 2 # If not set in Config or cmdline, then lets set to DEBUG
            else:
                LOGLEVEL = CFGLOGLEVEL # Config setting picked up

        CONFIG['LOGLEVEL'] = LOGLEVEL
        logger.alliancekeeper_log.initLogger(loglevel=CONFIG['LOGLEVEL'])
        logger.info("Log level set to [%s] - Log Directory is [%s] - Config level is [%s]" % (
            CONFIG['LOGLEVEL'], CONFIG['LOGDIR'], CFGLOGLEVEL))
        if CONFIG['LOGLEVEL'] > 2:
            logger.info("Screen Log set to FULL DEBUG")
        elif CONFIG['LOGLEVEL'] == 2:
            logger.info("Screen Log set to DEBUG")
        else:
            logger.info("Screen Log set to INFO/WARN/ERROR")

        config_read()
        if not os.path.exists(CONFIGFILE):
            config_write()
        logger.info('SYS_ENCODING is %s' % SYS_ENCODING)
        CACHEDIR = os.path.join(DATADIR, 'cache')
        try:
            os.makedirs(CACHEDIR)
        except OSError as e:
            if not os.path.isdir(CACHEDIR):
                logger.error('Could not create cachedir; %s' % e.strerror)
        SESSIONDIR = os.path.join(DATADIR, 'sessions')
        try:
            os.makedirs(SESSIONDIR)
        except OSError as e:
            if not os.path.isdir(SESSIONDIR):
                logger.error('Could not create sessiondir; %s' % e.strerror)


        time_now = int(time.time())

        # Initialize the database
        try:
            myDB = database.DBConnection()
            result = myDB.match('PRAGMA user_version')
            check = myDB.match('PRAGMA integrity_check')
            if result:
                version = result[0]
            else:
                version = 0
            logger.info("Database is version %s, integrity check: %s" % (version, check[0]))

        except Exception as e:
            logger.error("Can't connect to the database: %s" % str(e))

        BOOTSTRAP_THEMELIST = build_bootstrap_themes() # Add when bootstrap is added

        __INITIALIZED__ = True
        checkRunningJobs()
        return True

def config_read(reloaded=False):
    global CONFIG, CONFIG_DEFINITIONS, CONFIG_NONWEB, CONFIG_NONDEFAULT

    for key in CONFIG_DEFINITIONS.keys():
        item_type, section, default = CONFIG_DEFINITIONS[key]
        CONFIG[key.upper()] = check_setting(item_type, section, key.lower(), default)
    if not CONFIG['LOGDIR']:
        CONFIG['LOGDIR'] = os.path.join(DATADIR, 'logs')
    if CONFIG['HTTP_PORT'] < 21 or CONFIG['HTTP_PORT'] > 65535:
        CONFIG['HTTP_PORT'] = 8339

    myDB = database.DBConnection()
    # check if we have an active database yet, not a fresh install
    result = myDB.match('PRAGMA user_version')
    if result:
        version = result[0]
    else:
        version = 0

    if reloaded:
        logger.info('Config file reloaded')
    else:
        logger.info('Config file loaded')

def config_write():
    global CONFIG_NONWEB, CONFIG_NONDEFAULT, LOGLEVEL

    interface = CFG.get('General', 'http_look')
    for key in CONFIG_DEFINITIONS.keys():
        item_type, section, default = CONFIG_DEFINITIONS[key]
        if key == 'WALL_COLUMNS': # may be modified by user interface but not on config page
            value = CONFIG[key]
        elif key not in CONFIG_NONWEB and not (interface == 'default' and key in CONFIG_NONDEFAULT):
            check_section(section)
            value = CONFIG[key]
            if key == 'LOGLEVEL':
                LOGLEVEL = check_int(value,2)
            elif key in ['LOGDIR', 'DOWNLOAD_DIR']:
                value = value.encode(SYS_ENCODING)
        else:
            value = CFG.get(section, key.lower())
            if CONFIG['LOGLEVEL'] > 2:
                logger.debug("Leaving %s unchanged (%s)" % (key, value))
            CONFIG[key] = value
        CFG.set(section, key.lower(), value)

    # sanity check for typos
    for key in CONFIG.keys():
        if key not in CONFIG_DEFINITIONS.keys():
            logger.warn('Unsaved config key: %s' % key)

    # myDB = database.DBConnection() # Uncomment if any DB info needs written to the config

    msg = None
    try:
        with open(CONFIGFILE + '.new', 'wb') as configfile:
            CFG.write(configfile)
    except Exception as e:
        msg = '{} {} {}'.format('Unable to create new config file:', CONFIGFILE, e.strerror)
        logger.warn(msg)
        return

    try:
        os.remove(CONFIGFILE + '.bak')
    except OSError as e:
        if e.errno is not 2: # doesn't exist is ok
            msg = '{} {}{} {}'.format('Error deleting backup file:', CONFIGFILE, '.bak', e.strerror)
            logger.warn(msg)

    try:
        os.rename(CONFIGFILE, CONFIGFILE + '.bak')
    except OSError as e:
        if e.errno is not 2: # doesn't exist is ok as wouldn't exist until first save
            msg = '{} {} {}'.format('Unable to backup config file', CONFIGFILE, e.strerror)
            logger.warn(msg)

    try:
        os.rename(CONFIGFILE + '.new', CONFIGFILE)
    except OSError as e:
        msg = '{} {} {}'.format('Unable to rename new config file:', CONFIGFILE, e.strerror)
        logger.warn(msg)

    if not msg:
        msg = 'Config file [%s] has been updated' % CONFIGFILE
        logger.info(msg)

def build_bootstrap_themes():
    themelist = []
    if not os.path.isdir(os.path.join(PROG_DIR, 'data', 'interfaces', 'bootstrap')):
        return themelist  # return empty if bookstrap interface not installed

    URL = 'http://bootswatch.com/api/3.json'
    result, success = fetchURL(URL, None, False)  # use default headers, no retry

    if not success:
        logger.debug("Error getting bootstrap themes : %s" % result)
        return themelist

    try:
        results = json.loads(result)
        for theme in results['themes']:
            themelist.append(theme['name'].lower())
    except Exception as e:
        # error reading results
        logger.debug('JSON Error reading bootstrap themes, %s' % str(e))

    logger.debug("Bootstrap found %i themes" % len(themelist))
    return themelist

def daemonize():

    # make a non-session-leader child process
    try:
        pid = os.fork()
        if pid != 0:
            sys.exit(0)
    except OSError as e:
        raise RuntimeError("1st fork failed: %s [%d]" % (e.strerror, e.errno))

        os.setsid()

        prev = os.umask(0)
        os.umask(prev and int('077', 8))

        try:
            pid = os.fork()
            if pid != 0:
                sys.exit()
        except OSError as e:
            raise RuntimeError("2nd fork failed: %s [%d]" % (e.strerror, e.errno))

        dev_null = file('/dev/null', 'r')
        os.dup2(dev_null.fileno(), sys.stdin.fileno())

        if PIDFILE:
            pid = str(os.getpid())
            logger.debug(u"Writing PID " + pid + " to " + str(PIDFILE))
            file(PIDFILE, 'w').write("%s\n" % pid)

def start():
    global __INITIALIZED__, started

    if __INITIALIZED__:
        SCHED.start()
        started = True

def logmsg(level, msg):
    # log messages to logger if initialized, or print if not.
    if __INITIALIZED__:
        if level == 'error':
            logger.error(msg)
        elif level == 'debug':
            logger.debug(msg)
        elif level == 'warn':
            logger.warn(msg)
        else:
            logger.info(msg)
    else:
        print level.upper(), msg

def shutdown(restart=False, update=False):
    cherrypy. engine.exit()
    SCHED.shutdown(wait=False)

    if not restart and not update:
        logmsg('info', 'AllianceKeeper is shutting down...')

    if update:
        logmsg('info', 'AllianceKeeper is updating...')
        try:
            if versioncheck.update():
                logmsg('info', 'AllianceKeeper version updated')
                config_write()
        except Exception as e:
            logmsg('warn', 'AllianceKeeper failed to update: %s. Restarting.' % str(e))


    if PIDFILE:
        logmsg('info', 'Removing pidfile %s' % PIDFILE)
        os.remove(PIDFILE)

    if restart:
        logmsg('info', 'AllianceKeeper is restarting...')
        # Try to use running python
        executable = sys.executable
        if not executable:
            if platform.system() == "windows":
                params = ["where", "python2"]
                try:
                    executable = subprocess.check_output(params, stderr=subprocess.STDOUT).strip()
                except Exception as e:
                    logger.debug("where python2 failed: %s" % str(e))
            else:
                params = ["which", "python2"]
                try:
                    executable = subprocess.check_output(params, stderr=subprocess.STDOUT).strip()
                except Exception as e:
                    logger.debug("where python2 failed: %s" % str(e))

        if not executable:
            executable = 'python'

        popen_list = [executable, FULL_PATH]
        popen_list += ARGS
        if '--update' in popen_list:
            popen_list.remove('--update')
        if LOGLEVEL:
            if '--quiet' in popen_list:
                popen_list.remove('--quiet')
            if '-q' in popen_list:
                popen_list.remove('-q')

        logmsg('debug', 'Restarting AllianceKeeper with ' + str(popen_list))
        subprocess.Popen(popen_list, cwd=os.getcwd())

    logmsg('info', 'alliancekeeper is exiting')
    os._exit(0)
