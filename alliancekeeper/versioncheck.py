import os
import platform
import re
import socket
import subprocess
import tarfile
import threading
import time
import urllib2

import alliancekeeper
import lib.simplejson as simplejson
from alliancekeeper import logger, version
from alliancekeeper.common import USER_AGENT

def logmsg(level, msg):
    # log messages to logger if initialised, or print if not.
    if alliancekeeper.__INITIALIZED__:
        if level == 'error':
            logger.error(msg)
        elif level == 'info':
            logger.info(msg)
        elif level == 'debug':
            logger.debug(msg)
        elif level == 'warn':
            logger.warn(msg)
        else:
            logger.info(msg)
    else:
        print level.upper(), msg

def runGit(args):
    # Function to execute GIT commands taking care of error logging etc
    if alliancekeeper.CONFIG['GIT_PROGRAM']:
        git_locations = ['"' + alliancekeeper.CONFIG['GIT_PROGRAM'] + '"']
    else:
        git_locations = ['git']

    if platform.system().lower() == 'darwin':
        git_locations.append('/usr/local/git/bin/git')

    output = err = None

    for cur_git in git_locations:

        cmd = cur_git + ' ' + args

        try:
            logmsg('debug', '(RunGit)Trying to execute: "' + cmd + '" with shell in ' + alliancekeeper.PROG_DIR)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                 shell=True, cwd=alliancekeeper.PROG_DIR)
            output, err = p.communicate()
            logmsg('debug', '(RunGit)Git output: [%s]' % output.strip('\n'))

        except OSError:
            logmsg('debug', '(RunGit)Command ' + cmd + ' didn\'t work, couldn\'t find git')
            continue

        if 'not found' in output or "not recognized as an internal or external command" in output:
            logmsg('debug', '(RunGit)Unable to find git with command ' + cmd)
            logmsg('error', 'git not found - please ensure git executable is in your PATH')
            output = None
        elif 'fatal:' in output or err:
            logmsg('error', '(RunGit)Git returned bad info. Are you sure this is a git installation?')
            output = None
        elif output:
            break

    return output, err

def getInstallType():
    # need a way of detecting if we are running a windows .exe file
    # (which we can't upgrade)  rather than just running git or source on windows
    # We use a string in the version.py file for this
    # FUTURE:   Add a version number string in this file too?
    try:
        install = version.ALLIANCEKEEPER_VERSION.lower()
    except:
        install = 'unknown'

    if install in ['windows', 'win32build']:
        alliancekeeper.CONFIG['INSTALL_TYPE'] = 'win'
        alliancekeeper.CURRENT_BRANCH = 'Windows'

    elif install == 'package':  # deb, rpm, other non-upgradeable
        alliancekeeper.CONFIG['INSTALL_TYPE'] = 'package'
        alliancekeeper.CONFIG['GIT_BRANCH'] = 'Package'

    elif os.path.isdir(os.path.join(alliancekeeper.PROG_DIR, '.git')):
        alliancekeeper.CONFIG['INSTALL_TYPE'] = 'git'
        alliancekeeper.CONFIG['GIT_BRANCH'] = getCurrentGitBranch()
    else:
        alliancekeeper.CONFIG['INSTALL_TYPE'] = 'source'
        alliancekeeper.CONFIG['GIT_BRANCH'] = 'master'

    logmsg('debug', '(getInstallType) [%s] install detected. Setting Branch to [%s]' %
           (alliancekeeper.CONFIG['INSTALL_TYPE'], alliancekeeper.CONFIG['GIT_BRANCH']))

def getCurrentVersion():
    if alliancekeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(getCurrentVersion) Windows install - no update available')

        # Don't have a way to update exe yet, but don't want to set VERSION to None
        VERSION = 'Windows Install'

    elif alliancekeeper.CONFIG['INSTALL_TYPE'] == 'git':
        output, err = runGit('rev-parse HEAD')

        if not output:
            logmsg('error', '(getCurrentVersion) Couldn\'t find latest git installed version.')
            cur_commit_hash = 'GIT Cannot establish version'
        else:
            cur_commit_hash = output.strip()

            if not re.match('^[a-z0-9]+$', cur_commit_hash):
                logmsg('error', '(getCurrentVersion) Output doesn\'t look like a hash, not using it')
                cur_commit_hash = 'GIT invalid hash return'

        VERSION = cur_commit_hash

    elif alliancekeeper.CONFIG['INSTALL_TYPE'] in ['source', 'package']:

        version_file = os.path.join(alliancekeeper.PROG_DIR, 'version.txt')

        if not os.path.isfile(version_file):
            VERSION = 'No Version File'
            logmsg('debug', '(getCurrentVersion) [%s] missing.' % version_file)
            return VERSION
        else:
            fp = open(version_file, 'r')
            current_version = fp.read().strip(' \n\r')
            fp.close()

            if current_version:
                VERSION = current_version
            else:
                VERSION = 'No Version set in file'
                return VERSION
    else:
        logmsg('error', '(getCurrentVersion) Install Type not set - cannot get version value')
        VERSION = 'Install type not set'
        return VERSION

    updated = updateVersionFile(VERSION)
    if updated:
        logmsg('debug', '(getCurrentVersion) - Install type [%s] Local Version is set to [%s] ' % (
            alliancekeeper.CONFIG['INSTALL_TYPE'], VERSION))
    else:
        logmsg('debug', '(getCurrentVersion) - Install type [%s] Local Version is unchanged [%s] ' % (
            alliancekeeper.CONFIG['INSTALL_TYPE'], VERSION))

    return VERSION

def getCurrentGitBranch():
    # Can only work for GIT driven installs, so check install type
    if alliancekeeper.CONFIG['INSTALL_TYPE'] != 'git':
        logmsg('debug', 'Non GIT Install doing getCurrentGitBranch')
        return 'NON GIT INSTALL'

    # use git rev-parse --abbrev-ref HEAD which returns the name of the current branch
    current_branch, err = runGit('rev-parse --abbrev-ref HEAD')
    current_branch = str(current_branch)
    current_branch = current_branch.strip(' \n\r')

    if not current_branch:
        logmsg('error', 'failed to return current branch value')
        return 'InvalidBranch'

    logmsg('debug', '(getCurrentGitBranch) Current local branch of repo is [%s] ' % current_branch)

    return current_branch

def checkForUpdates():
    """ Called at startup, from webserver with thread name WEBSERVER, or as a cron job """
    if 'Thread-' in threading.currentThread().name:
        threading.currentThread().name = "CRON-VERSIONCHECK"
    logmsg('debug', 'Set Install Type, Current & Latest Version and Commit status')
    getInstallType()
    alliancekeeper.CONFIG['CURRENT_VERSION'] = getCurrentVersion().encode('ascii')
    alliancekeeper.CONFIG['LATEST_VERSION'] = getLatestVersion().encode('ascii')
    if alliancekeeper.CONFIG['CURRENT_VERSION'] == alliancekeeper.CONFIG['LATEST_VERSION']:
        alliancekeeper.CONFIG['COMMITS_BEHIND'] = 0
        alliancekeeper.COMMIT_LIST = ""
    else:
        alliancekeeper.CONFIG['COMMITS_BEHIND'], alliancekeeper.COMMIT_LIST = getCommitDifferenceFromGit()
    logmsg('debug', 'Update check complete')

def getLatestVersion():
    if alliancekeeper.CONFIG['INSTALL_TYPE'] in ['git', 'source', 'package']:
        latest_version = getLatestVersion_FromGit()
    elif alliancekeeper.CONFIG['INSTALL_TYPE'] in ['win']:
        latest_version = 'WINDOWS INSTALL'
    else:
        latest_version = 'UNKNOWN INSTALL'

    alliancekeeper.CONFIG['LATEST_VERSION'] = latest_version
    return latest_version

def getLatestVersion_FromGit():
    latest_version = 'Unknown'

    # Can only work for non Windows driven installs, so check install type
    if alliancekeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(getLatestVersion_FromGit) Error - should not be called under a windows install')
        latest_version = 'WINDOWS INSTALL'
    else:
        # check current branch value of the local git repo as folks may pull from a branch not master
        branch = alliancekeeper.CONFIG['GIT_BRANCH']

        if branch == 'InvalidBranch':
            logmsg('debug', '(getLatestVersion_FromGit) - Failed to get a valid branch name from local repo')
        else:
            if branch == 'Package':  # check packages against master
                branch = 'master'
            # Get the latest commit available from github
            url = 'https://api.bitbucket.org/2.0/repositories/%s/%s/commits/%s' % (
                alliancekeeper.CONFIG['GIT_USER'], alliancekeeper.CONFIG['GIT_REPO'], branch)
            logmsg('debug',
                   '(getLatestVersion_FromGit) Retrieving latest version information from github command=[%s]' % url)

            age = alliancekeeper.CONFIG['GIT_UPDATED']
            try:
                request = urllib2.Request(url)
                request.add_header('User-Agent', USER_AGENT)
                if age:
                    logmsg('debug', '(getLatestVersion_FromGit) Checking if modified since %s' % age)
                    request.add_header('If-Modified-Since', age)
                resp = urllib2.urlopen(request, timeout=30)
                result = resp.read()
                git = simplejson.JSONDecoder().decode(result)
                latest_version = git['values'][0]['hash']
                logmsg('debug', '(getLatestVersion_FromGit) Branch [%s] Latest Version has been set to [%s]' % (
                    branch, latest_version))
            except Exception as e:
                if hasattr(e, 'reason'):
                    errmsg = e.reason
                else:
                    errmsg = str(e)

                if hasattr(e, 'code') and str(e.code) == '304':  # Not modified
                    latest_version = alliancekeeper.CONFIG['CURRENT_VERSION']
                    logmsg('debug', '(getLatestVersion_FromGit) Not modified, currently on Latest Version')
                    # alliancekeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
                else:
                    logmsg('warn', '(getLatestVersion_FromGit) Could not get the latest commit from github')
                    logmsg('debug', 'git error for %s: %s' % (url, errmsg))
                    latest_version = 'Not_Available_From_GitHUB'

    return latest_version

def getCommitDifferenceFromGit():
    # See how many commits behind we are
    commits = -1
    # Takes current latest version value and trys to diff it with the latest
    # version in the current branch.
    commit_list = ''
    if alliancekeeper.CONFIG['LATEST_VERSION'] == 'Not_Available_From_GitHUB':
        commits = 0  # don't report a commit diff as we don't know anything
    if alliancekeeper.CONFIG['CURRENT_VERSION'] and commits != 0:
        logmsg('info', '[VersionCheck] -  Comparing currently installed version with latest github version')
        url = 'https://api.bitbucket.org/2.0/repositories/%s/%s/commits/%s' % (
            alliancekeeper.CONFIG['GIT_USER'], alliancekeeper.CONFIG['GIT_REPO'], alliancekeeper.CONFIG['GIT_BRANCH'])
        logmsg('debug', '(getCommitDifferenceFromGit) -  Check for differences between local & repo by [%s]' % url)

        try:
            request = urllib2.Request(url)
            request.add_header('User-Agent', USER_AGENT)
            resp = urllib2.urlopen(request, timeout=30)
            result = resp.read()
            try:
                logmsg('debug', 'JSONDecode url')
                git = simplejson.JSONDecoder().decode(result)
            except Exception:
                logmsg('warn', '(getCommitDifferenceFromGit) -  could not get difference status from GitHub')
            # logger.debug(git['values'])
            commits = 0
            messages = []
            newer = True
            while newer:
                thiscommit = git['values'][commits]
                logger.debug(thiscommit['hash'])
                if thiscommit['hash'].encode('ascii') == alliancekeeper.CONFIG['CURRENT_VERSION']:
                    newer = False
                else:
                    commits += 1
                    messages.insert(0, thiscommit['message'])
            for line in messages:
                commit_list = "%s\n%s" % (commit_list, line)
        except Exception:
            msg = '(getCommitDifferenceFromGit) -  Could not get commits behind from github. '
            msg += 'Can happen if you have a local commit not pushed to repo or no connection to github'
            logmsg('warn', msg)

        if commits > 1:
            logmsg('info', '[VersionCheck] -  New version is available. You are %s commits behind' % commits)
        elif commits == 1:
            logmsg('info', '[VersionCheck] -  New version is available. You are one commit behind')
        elif commits == 0:
            logmsg('info', '[VersionCheck] -  alliancekeeper is up to date ')
            # alliancekeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
        elif commits < 0:
            msg = '[VersionCheck] -  You are running an unknown version of alliancekeeper. '
            msg += 'Run the updater to identify your version'
            logmsg('info', msg)

    elif alliancekeeper.CONFIG['LATEST_VERSION'] == 'Not_Available_From_GitHUB':
        commit_list = 'Unable to get latest version from GitHub'
        logmsg('info', commit_list)
    else:
        logmsg('info', 'You are running an unknown version of alliancekeeper. Run the updater to identify your version')

    logmsg('debug', '(getCommitDifferenceFromGit) - exiting with commit value of [%s]' % commits)
    # alliancekeeper.CONFIG['COMMITS_BEHIND'] = commits
    return commits, commit_list


def updateVersionFile(new_version_id):
    # Update version.txt located in AK home dir.
    version_path = os.path.join(alliancekeeper.PROG_DIR, 'version.txt')

    try:
        logmsg('debug', "(updateVersionFile) Updating [%s] with value [%s]" % (
            version_path, new_version_id))
        if os.path.exists(version_path):
            ver_file = open(version_path, 'r')
            current_version = ver_file.read().strip(' \n\r')
            ver_file.close()
            if current_version == new_version_id:
                return False

        ver_file = open(version_path, 'w')
        ver_file.write(new_version_id)
        ver_file.close()
        alliancekeeper.CONFIG['CURRENT_VERSION'] = new_version_id
        return True
    except IOError as e:
        logmsg('error',
               u"(updateVersionFile) Unable to write current version to version.txt, update not complete: %s" % str(e))
        return False



def update():
    if alliancekeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(update) Windows install - no update available')
        logmsg('info', '(update) Windows .exe updating not supported yet.')
        return False
    elif alliancekeeper.CONFIG['INSTALL_TYPE'] == 'package':
        logmsg('debug', '(update) Package install - no update available')
        logmsg('info', '(update) Please use your package manager to update')
        return False

    elif alliancekeeper.CONFIG['INSTALL_TYPE'] == 'git':
        branch = getCurrentGitBranch()

        _, _ = runGit('stash clear')
        output, err = runGit('pull origin ' + branch)

        success = True
        if not output:
            logmsg('error', '(update) Couldn\'t download latest version')
            success = False
        for line in output.split('\n'):
            if 'Already up-to-date.' in line:
                logmsg('info', '(update) No update available, not updating')
                logmsg('info', '(update) Output: ' + str(output))
                success = False
                # alliancekeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
            elif 'Aborting' in line or 'local changes' in line:
                logmsg('error', '(update) Unable to update from git: ' + line)
                logmsg('info', '(update) Output: ' + str(output))
                success = False
        if success:
            alliancekeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
            return True
    # elif alliancekeeper.CONFIG['INSTALL_TYPE'] == 'source':
    #
    #     # As this is a non GIT install, we assume that the comparison is
    #     # always to master.
    #
    #     tar_download_url = 'https://github.com/%s/%s/tarball/%s' % (
    #         alliancekeeper.CONFIG['GIT_USER'], alliancekeeper.CONFIG['GIT_REPO'], alliancekeeper.CONFIG['GIT_BRANCH'])
    #     update_dir = os.path.join(alliancekeeper.PROG_DIR, 'update')
    #     # version_path = os.path.join(alliancekeeper.PROG_DIR, 'version.txt')
    #
    #     try:
    #         logmsg('info', '(update) Downloading update from: ' + tar_download_url)
    #         request = urllib2.Request(tar_download_url)
    #         request.add_header('User-Agent', USER_AGENT)
    #         data = urllib2.urlopen(request, timeout=30)
    #     except socket.timeout:
    #         logmsg('error', "(update) Timeout retrieving new version from " + tar_download_url)
    #         return False
    #     except (urllib2.HTTPError, urllib2.URLError) as e:
    #         if hasattr(e, 'reason'):
    #             errmsg = e.reason
    #         else:
    #             errmsg = str(e)
    #         logmsg('error',
    #                "(update) Unable to retrieve new version from " + tar_download_url + ", can't update: %s" % errmsg)
    #         return False
    #
    #     download_name = data.geturl().split('/')[-1]
    #
    #     tar_download_path = os.path.join(alliancekeeper.PROG_DIR, download_name)
    #
    #     # Save tar to disk
    #     f = open(tar_download_path, 'wb')
    #     f.write(data.read())
    #     f.close()
    #
    #     # Extract the tar to update folder
    #     logmsg('info', '(update) Extracting file' + tar_download_path)
    #     tar = tarfile.open(tar_download_path)
    #     tar.extractall(update_dir)
    #     tar.close()
    #
    #     # Delete the tar.gz
    #     logmsg('info', '(update) Deleting file' + tar_download_path)
    #     os.remove(tar_download_path)
    #
    #     # Find update dir name
    #     update_dir_contents = [x for x in os.listdir(update_dir) if os.path.isdir(os.path.join(update_dir, x))]
    #     if len(update_dir_contents) != 1:
    #         logmsg('error', u"(update) Invalid update data, update failed: " + str(update_dir_contents))
    #         return False
    #     content_dir = os.path.join(update_dir, update_dir_contents[0])
    #
    #     # walk temp folder and move files to main folder
    #     for dirname, dirnames, filenames in os.walk(content_dir):
    #         dirname = dirname[len(content_dir) + 1:]
    #         for curfile in filenames:
    #             old_path = os.path.join(content_dir, dirname, curfile)
    #             new_path = os.path.join(alliancekeeper.PROG_DIR, dirname, curfile)
    #
    #             if os.path.isfile(new_path):
    #                 os.remove(new_path)
    #             os.renames(old_path, new_path)
    #
    #     # Update version.txt
    #     updateVersionFile(alliancekeeper.CONFIG['LATEST_VERSION'])
    #     # alliancekeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
    #     return True
    else:
        logmsg('error', "(update) Cannot perform update - Install Type not set")
        return False


