#!/usr/bin/env python2
# -*- coding: UTF-8 -*-
import ConfigParser
import locale
import os
import platform
import stat
import sys
import threading
import time
import re

import alliancekeeper
from alliancekeeper import webStart, logger, dbupgrade, versioncheck

def main():
    # rename this thread
    threading.currentThread().name = "MAIN"

    # Set paths
    if hasattr(sys, 'frozen'):
        alliancekeeper.FULL_PATH = os.path.abspath(sys.executable)
    else:
        alliancekeeper.FULL_PATH = os.path.abspath(__file__)

    alliancekeeper.PROG_DIR = os.path.dirname(alliancekeeper.FULL_PATH)
    alliancekeeper.ARGS = sys.argv[1:]

    alliancekeeper.SYS_ENCODING = None

    try:
        locale.setlocale(locale.LC_ALL, "")
        alliancekeeper.SYS_ENCODING = 'utf-8'
    except (locale.Error, IOError):
        pass

    # set arguments

    from optparse import OptionParser

    p = OptionParser()
    p.add_option('-d', '--daemon', action="store_true", dest='daemon', help="Run the server as a daemon")
    p.add_option('-q', '--quiet', action="store_true", dest='quiet', help="Don't log to the console")
    p.add_option('--debug', action="store_true", dest='debug', help="Show debuglog messages")
    p.add_option('--update', action="store_true", dest='update', help="Update to latest version (only git or source installs)")
    p.add_option('--port', dest='port', default=None, help="Force webinterface to listen on this port")
    p.add_option('--datadir', dest='datadir', default=None, help="Path to the data directory")
    p.add_option('--config', dest='config', default=None, help="Path to config.ini file")
    p.add_option('-p', '--pidfile', dest='pidfile', default=None, help="Store the process id in the given file")

    options, args = p.parse_args()

    alliancekeeper.LOGLEVEL = 1
    if options.debug:
        alliancekeeper.LOGLEVEL = 3
    if options.quiet:
        alliancekeeper.LOGLEVEL = 0
    if options.daemon:
        if 'windows' not in platform.system().lower():
            alliancekeeper.DAEMON = True
            alliancekeeper.daemonize()
        else:
            print("Daemonize not supported under Windows, starting normally")

    if options.update:
        alliancekeeper.SIGNAL = 'update'
        # This is the "emergency recovery" update in case alliancekeeper won't start.
        # Set up some dummy values for the update as we have not read the config file yet
        alliancekeeper.CONFIG['GIT_PROGRAM'] = ''
        alliancekeeper.CONFIG['GIT_USER'] = 'DarkSir23'
        alliancekeeper.CONFIG['GIT_REPO'] = 'alliancekeeper'
        alliancekeeper.CONFIG['LOGLIMIT'] = 2000
        versioncheck.getInstallType()
        if alliancekeeper.CONFIG['INSTALL_TYPE'] not in ['git', 'source']:
            alliancekeeper.SIGNAL = None
            print('Cannot update, not a git or source installation')
        else:
            alliancekeeper.shutdown(restart=True, update=True)

    if options.datadir:
        alliancekeeper.DATADIR = str(options.datadir)
    else:
        alliancekeeper.DATADIR = alliancekeeper.PROG_DIR

    if options.config:
        alliancekeeper.CONFIGFILE = str(options.config)
    else:
        alliancekeeper.CONFIGFILE = os.path.join(alliancekeeper.DATADIR, "config.ini")

    if options.pidfile:
        if alliancekeeper.DAEMON:
            alliancekeeper.PIDFILE = str(options.pidfile)

    # create and check (optional) paths
    if not os.path.exists(alliancekeeper.DATADIR):
        try:
            os.makedirs(alliancekeeper.DATADIR)
        except OSError:
            raise SystemExit('Could not create data directory: ' + alliancekeeper.DATADIR + '. Exit ...')

    if not os.access(alliancekeeper.DATADIR, os.W_OK):
        raise SystemExit('Could not write to the data directory: ' + alliancekeeper.DATADOR + '. Exit ...')

    print "alliancekeeper is starting up..."
    time.sleep(4) # allow a bit of time for old task to exit if restarting.  Needs to free logfile and server port.

    #create database and config
    alliancekeeper.DBFILE = os.path.join(alliancekeeper.DATADIR, 'alliancekeeper.db')
    alliancekeeper.CFG = ConfigParser.RawConfigParser()
    alliancekeeper.CFG.read(alliancekeeper.CONFIGFILE)

    # No Logging Before This Point
    alliancekeeper.initialize()

    # Add Versioncheck here
    versioncheck.checkForUpdates()

    if options.port:
        alliancekeeper.CONFIG['HTTP_PORT'] = int(options.port)
        logger.info('Starting alliancekeeper on forced port: %s, webroot "%s"' % (alliancekeeper.CONFIG['HTTP_PORT'], alliancekeeper.CONFIG['HTTP_ROOT']))
    else:
        alliancekeeper.CONFIG['HTTP_PORT'] = int(alliancekeeper.CONFIG['HTTP_PORT'])
        logger.info('Starting alliancekeeper on port: %s, webroot "%s"' % (alliancekeeper.CONFIG['HTTP_PORT'], alliancekeeper.CONFIG['HTTP_ROOT']))

    if alliancekeeper.DAEMON:
        alliancekeeper.daemonize()

    curr_ver = dbupgrade.upgrade_needed()
    if curr_ver:
        alliancekeeper.UPDATE_MSG = 'Updating database to version %s' % curr_ver


    # Try to start the server

    webStart.initialize({
        'http_port': alliancekeeper.CONFIG['HTTP_PORT'],
        'http_host': alliancekeeper.CONFIG['HTTP_HOST'],
        'http_root': alliancekeeper.CONFIG['HTTP_ROOT'],
        'http_user': alliancekeeper.CONFIG['HTTP_USER'],
        'http_pass': alliancekeeper.CONFIG['HTTP_PASS'],
        'http_proxy': alliancekeeper.CONFIG['HTTP_PROXY'],
        'https_enabled': alliancekeeper.CONFIG['HTTPS_ENABLED'],
        'https_cert': alliancekeeper.CONFIG['HTTPS_CERT'],
        'https_key': alliancekeeper.CONFIG['HTTPS_KEY']
    })

    if curr_ver:
        threading.Thread(target=dbupgrade.dbupgrade, name="DB_UPGRADE", args=[curr_ver]).start()

    alliancekeeper.start()

    while True:
        if not alliancekeeper.SIGNAL:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                alliancekeeper.shutdown()
        else:
            if alliancekeeper.SIGNAL == 'shutdown':
                alliancekeeper.shutdown()
            elif alliancekeeper.SIGNAL == 'restart':
                alliancekeeper.shutdown(restart=True)
            elif alliancekeeper.SIGNAL == 'update':
                alliancekeeper.shutdown(restart=True, update=True)
            alliancekeeper.SIGNAL = None

if __name__ == "__main__":
    main()
